package com.sujan.code.nycschools;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)

public class DetailsActivityTest {


    ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule(DetailsActivity.class);

    @Test
    public void VerifyAllViews_Displayed() {
        activityTestRule.launchActivity(new Intent());
        onView(withId(R.id.name)).check(matches(isEnabled()));
        onView(withId(R.id.overview)).check(matches(isDisplayed()));
        onView(withId(R.id.satReading)).check(matches(isDisplayed()));
        onView(withId(R.id.satWriting)).check(matches(isDisplayed()));
        onView(withId(R.id.satMath)).check(matches(isDisplayed()));
        onView(withId(R.id.contact)).check(matches(isDisplayed()));
        onView(withId(R.id.address)).check(matches(isDisplayed()));
        onView(withId(R.id.email)).check(matches(isDisplayed()));
        onView(withId(R.id.phone)).check(matches(isDisplayed()));


    }

}
