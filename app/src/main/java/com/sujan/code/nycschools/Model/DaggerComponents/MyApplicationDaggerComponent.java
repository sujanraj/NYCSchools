package com.sujan.code.nycschools.Model.DaggerComponents;


import com.sujan.code.nycschools.Model.RetrofitComponents.NYCOpenDataParser;
import com.sujan.code.nycschools.Presenter.MainPresenter;
import com.sujan.code.nycschools.View.HomeFragment;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {MyApplicationDaggerModule.class})
public interface MyApplicationDaggerComponent {

    void inject(MainPresenter mainPresenter);

    void inject(NYCOpenDataParser NYCOpenDataParser);

    void inject(HomeFragment homeFragment);

}
