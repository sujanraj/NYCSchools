package com.sujan.code.nycschools.View;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SchoolsData;
import com.sujan.code.nycschools.Presenter.MainPresenter;
import com.sujan.code.nycschools.R;

import java.util.List;

/**
 * SchoolsRecyclerViewAdapter is the RecyclerViewAdapter.
 * It will set the Layout file for the View in RecyclerView.
 * Gives the values to views from the data received from Network response.
 */
public class SchoolsRecyclerViewAdapter extends RecyclerView.Adapter<SchoolsRecyclerViewHolder> {

    MainPresenter mainPresenter;
    private List<SchoolsData> responseList;

    // Getting the Data from the Network Response.
    public SchoolsRecyclerViewAdapter(List<SchoolsData> responseList, MainPresenter mainPresenter) {
        this.responseList = responseList;
        this.mainPresenter = mainPresenter;
    }


    // Inflating the Layout for the RecyclerView
    @Override
    public SchoolsRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.datalayout, parent, false);
        return new SchoolsRecyclerViewHolder(view, mainPresenter);
    }


    @Override
    public void onBindViewHolder(SchoolsRecyclerViewHolder holder, int position) {
        Log.e("###", "size " + responseList.get(position).getSchool_name());
        holder.time.setText(responseList.get(position).getSchool_name());
        holder.dbn.setText(responseList.get(position).getDbn());
        holder.location.setText(responseList.get(position).getLocation());
        holder.phone.setText(responseList.get(position).getPhone_number());
        holder.weblink.setText(responseList.get(position).getWebsite());

    }


    // Passing the Required number of views for the RecyclerView
    @Override
    public int getItemCount() {
        return responseList.size();
    }
}
