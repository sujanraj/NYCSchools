package com.sujan.code.nycschools.Utils;

/**
 * Created by macbookpro on 3/26/18.
 */

public interface ILoadMainFragment {
    public void loadMainFragment(boolean connectionStatus);
}
