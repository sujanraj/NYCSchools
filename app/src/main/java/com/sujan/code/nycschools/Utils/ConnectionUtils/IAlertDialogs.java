package com.sujan.code.nycschools.Utils.ConnectionUtils;

/**
 * Created by macbookpro on 3/26/18.
 */

public interface IAlertDialogs {
    void showNoConnection(String title, String message);

    void showSnackBarAlert(String message);

}
