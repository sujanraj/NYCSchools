package com.sujan.code.nycschools.View;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sujan.code.nycschools.Presenter.MainPresenter;
import com.sujan.code.nycschools.R;


/**
 * SchoolsRecyclerViewHolder is the RecyclerViewViewHolder.
 * It will contain the  views of Layout file used in RecyclerView.
 */

public class SchoolsRecyclerViewHolder extends RecyclerView.ViewHolder {
    TextView dbn, time, location, phone, weblink;


    public SchoolsRecyclerViewHolder(final View itemView, final MainPresenter mainPresenter) {
        super(itemView);
        dbn = itemView.findViewById(R.id.duration);
        time = itemView.findViewById(R.id.time);
        location = itemView.findViewById(R.id.location);
        phone = itemView.findViewById(R.id.phonenum);
        weblink = itemView.findViewById(R.id.weblink);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.requestDetailsAPIcall(dbn.getText().toString());
            }
        });
    }

}
