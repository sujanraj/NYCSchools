package com.sujan.code.nycschools;

import android.app.SearchManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.sujan.code.nycschools.Model.RetrofitComponents.NYCOpenDataEvent;
import com.sujan.code.nycschools.Model.RetrofitComponents.NYCOpenDataFailureEvent;
import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.DetailResponseEvent;
import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SATRecords;
import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SchoolsData;
import com.sujan.code.nycschools.Presenter.MainPresenter;
import com.sujan.code.nycschools.Utils.ConnectionUtils.IAlertDialogs;
import com.sujan.code.nycschools.Utils.ILoadMainFragment;
import com.sujan.code.nycschools.Utils.PresenterViewModel;
import com.sujan.code.nycschools.View.HomeFragment;
import com.sujan.code.nycschools.View.MainFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity implements ILoadMainFragment, IAlertDialogs {


    private MainFragment mMainFragment;
    private FragmentManager fragmentManager;
    private HomeFragment homeFragment;
    private SearchView searchView = null;
    private CoordinatorLayout coordinatorLayout;
    private PresenterViewModel presenterViewModel;
    private MainPresenter mainPresenter;
    private FragmentTransaction fragmentTransaction;
    private MenuItem searchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        coordinatorLayout = findViewById(R.id.main);
        fragmentManager = getSupportFragmentManager();
        presenterViewModel = ViewModelProviders.of(this).get(PresenterViewModel.class);
        mainPresenter = presenterViewModel.getMainPresenter();
        mainPresenter.buildDagger();
        homeFragment = new HomeFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainfragment, homeFragment);
        fragmentTransaction.commit();


    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
        }
        searchView.setVisibility(View.INVISIBLE);
        searchItem.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Refreshing the UI with Updated Network Response
            case R.id.refresh:
                mainPresenter.makeOpenNotifyAPIcall();
                mMainFragment.getSchoolsRecyclerViewAdapter().notifyDataSetChanged();
                return true;

            default:
                break;
        }

        return false;
    }


    /**
     * Shows alert dialog if there is no internet connection.
     *
     * @param title   title of the alert dialog.
     * @param message message to explain that there is no internet connection.
     */
    public void showNoConnection(String title, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setPositiveButton(("Open Settings"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivity(myIntent);
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Log.e("..", "Cancel button is clicked.");
                showSnackBarAlert("Internet connection is required to proceed.");

            }
        });
        dialog.show();
    }

    /**
     * SnackBar message to show that internet connection is required
     *
     * @param message message to show in the snackBar
     */
    @Override
    public void showSnackBarAlert(String message) {
        Snackbar snackbar = Snackbar
                .make(this.findViewById(R.id.main), message, Snackbar.LENGTH_INDEFINITE)
                .setAction("Allow Internet", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_WIFI_SETTINGS);
                        startActivity(intent);
                    }
                });

        snackbar.show();

    }


    /**
     * This method is invoked when request is successfull.
     * Event is fired from NYCOpenDataParse.getSchoolDetailsFromAPI(final String dbn).
     *
     * @param NYCOpenDataEvent Custom event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSuccessEvent(NYCOpenDataEvent NYCOpenDataEvent) {
        presenterViewModel.setSchoolsData(NYCOpenDataEvent.getListOfResponse());
        mMainFragment.onSuccessOpenNotifyApiResponse(presenterViewModel.getSchoolsData());

        searchTextChangeObservable();


    }


    /**
     * This method is invoked when request fails.
     *
     * @param NYCOpenDataFailureEvent Custom failure event.
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFailureEvent(NYCOpenDataFailureEvent NYCOpenDataFailureEvent) {
        // Passing the Network Response to UI
        showSnackBarAlert("Internet connection is required to proceed.");
        mMainFragment.onFailureOpenNotifyApiResponse(NYCOpenDataFailureEvent.getOnFailureResponse());

    }


    /**
     * Listen's to response from the API end point. Once school's detail is received, this method is fired.
     * Event is fired from NYCOpenDataParse.getSchoolDetailsFromAPI(final String dbn).
     *
     * @param response response from the Restful request
     */
    @Subscribe
    public void onReceiveSchoolDetails(DetailResponseEvent response) {
        Intent intent = new Intent(this, DetailsActivity.class);
        for (SATRecords s : response.getResponse()) {
            if (response.getDbn().equals(s.getDbn())) {
                intent.putExtra("SATReadingAvg", s.getSat_critical_reading_avg_score());
                intent.putExtra("SATMathAvg", s.getSat_math_avg_score());
                intent.putExtra("SATWritingAvg", s.getSat_writing_avg_score());
            }
        }
        for (SchoolsData s : presenterViewModel.getSchoolsData()) {
            if (s.getDbn().equals(response.getDbn())) {
                intent.putExtra("overview", s.getOverview_paragraph());
                intent.putExtra("schoolName", s.getSchool_name());
                intent.putExtra("address", s.getLocation());
                intent.putExtra("phone", s.getPhone_number());
                intent.putExtra("email", s.getSchool_email());
            }
        }
        startActivity(intent);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    /**
     * Creates RXJava observable of textinput events in the searchview.
     * If user inputs text in search view, observables are emitted, this subscribes to the changes
     * and notify recycler view to update.
     *
     * @return Observables that are text inputs in the searchview
     */
    private void searchTextChangeObservable() {
        Observable<String> observable = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull final ObservableEmitter<String> e) throws Exception {
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        e.onNext(newText);
                        return true;
                    }
                });
            }
        });
        observable.observeOn(Schedulers.io())
                .map(new Function<String, List<SchoolsData>>() {
                    @Override
                    public List<SchoolsData> apply(String s) throws Exception {
                        List<SchoolsData> searchresult = new ArrayList<>();
                        for (SchoolsData a : presenterViewModel.getSchoolsData()) {
                            if (a.getSchool_name().startsWith(s)) {
                                searchresult.add(a);
                            }
                        }
                        return searchresult;
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<SchoolsData>>() {
                    @Override
                    public void accept(List<SchoolsData> schoolsData) throws Exception {
                        mMainFragment.onSuccessOpenNotifyApiResponse(schoolsData);

                    }
                });

    }

    /**
     * load main fragment once internet connection is available in the device.
     *
     * @param connectionStatus internet status
     */
    @Override
    public void loadMainFragment(boolean connectionStatus) {
        if (connectionStatus) {
            mMainFragment = new MainFragment();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mainfragment, mMainFragment);
            fragmentTransaction.commit();
            searchView.setVisibility(View.VISIBLE);
            searchItem.setVisible(true);

        } else {

        }
    }


}
