package com.sujan.code.nycschools.Model.RetrofitComponents;


import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SATRecords;
import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SchoolsData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * NYCOpenDataInterface is the client interface for the Retrofit instance.
 * It contains dynamic part of the URL and methods to complete the network call.
 */

public interface NYCOpenDataInterface {
    //    Passing the latitude and longitude values of the Device and completing the URL to make the network call.
    @GET("97mf-9njv.json")
    Call<List<SchoolsData>> getSchoolData();

    @GET("734v-jeq5.json")
    Call<List<SATRecords>> getSchoolSATData();
}