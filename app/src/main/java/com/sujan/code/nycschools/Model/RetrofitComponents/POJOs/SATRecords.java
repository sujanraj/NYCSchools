package com.sujan.code.nycschools.Model.RetrofitComponents.POJOs;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by macbookpro on 3/23/18.
 */

public class SATRecords implements Parcelable {


    public static final Creator<SATRecords> CREATOR = new Creator<SATRecords>() {
        @Override
        public SATRecords createFromParcel(Parcel in) {
            return new SATRecords(in);
        }

        @Override
        public SATRecords[] newArray(int size) {
            return new SATRecords[size];
        }
    };
    /**
     * dbn : 01M292
     * num_of_sat_test_takers : 29
     * sat_critical_reading_avg_score : 355
     * sat_math_avg_score : 404
     * sat_writing_avg_score : 363
     * school_name : HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES
     */

    private String dbn;
    private String num_of_sat_test_takers;
    private String sat_critical_reading_avg_score;
    private String sat_math_avg_score;
    private String sat_writing_avg_score;
    private String school_name;

    protected SATRecords(Parcel in) {
        dbn = in.readString();
        num_of_sat_test_takers = in.readString();
        sat_critical_reading_avg_score = in.readString();
        sat_math_avg_score = in.readString();
        sat_writing_avg_score = in.readString();
        school_name = in.readString();
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getNum_of_sat_test_takers() {
        return num_of_sat_test_takers;
    }

    public void setNum_of_sat_test_takers(String num_of_sat_test_takers) {
        this.num_of_sat_test_takers = num_of_sat_test_takers;
    }

    public String getSat_critical_reading_avg_score() {
        return sat_critical_reading_avg_score;
    }

    public void setSat_critical_reading_avg_score(String sat_critical_reading_avg_score) {
        this.sat_critical_reading_avg_score = sat_critical_reading_avg_score;
    }

    public String getSat_math_avg_score() {
        return sat_math_avg_score;
    }

    public void setSat_math_avg_score(String sat_math_avg_score) {
        this.sat_math_avg_score = sat_math_avg_score;
    }

    public String getSat_writing_avg_score() {
        return sat_writing_avg_score;
    }

    public void setSat_writing_avg_score(String sat_writing_avg_score) {
        this.sat_writing_avg_score = sat_writing_avg_score;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dbn);
        dest.writeString(num_of_sat_test_takers);
        dest.writeString(sat_critical_reading_avg_score);
        dest.writeString(sat_math_avg_score);
        dest.writeString(sat_writing_avg_score);
        dest.writeString(school_name);
    }
}
