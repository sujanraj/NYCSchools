package com.sujan.code.nycschools.View;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sujan.code.nycschools.MainActivity;
import com.sujan.code.nycschools.Model.DaggerComponents.MyApplicationDaggerBuild;
import com.sujan.code.nycschools.R;
import com.sujan.code.nycschools.Utils.ConnectionUtils.CheckNetwork;

import javax.inject.Inject;


/**
 * Created by macbookpro on 3/26/18.
 */

public class HomeFragment extends Fragment {


    private static final int REQUEST_WIFI_SETTING = '1';
    ProgressBar progressBar;
    TextView connectionUpdate;
    MyTask mBackgroundTask;
    boolean isTaskExecuting = false;

    @Inject
    CheckNetwork checkNetwork;

    private boolean network_connection = false;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //build dagger and inject the component
        mBackgroundTask = new MyTask();
        MyApplicationDaggerBuild.getMyApplicationDaggerBuild().getMyApplicationDaggerComponent().inject(this);
        setRetainInstance(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ui_fragment_home, container, false);
        progressBar = view.findViewById(R.id.hProgressBar);
        connectionUpdate = view.findViewById(R.id.updatetext);
        if (savedInstanceState != null) {
            int progress = savedInstanceState.getInt("progress_value");
            progressBar.setProgress(progress);
        }
        Log.e("...", "onCreate");

        return view;
    }


    /**
     * This method is called before an activity may be killed Store info in
     * bundle if required.
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("progress_value", progressBar.getProgress());
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("...", "onStart");
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.e("...", "onResume");
        startBackgroundTask();

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("...", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("...", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("...", "HOMEFRAGMENTonDestroy");
    }

    public void startBackgroundTask() {
        if (!isTaskExecuting) {

            mBackgroundTask.execute();
            isTaskExecuting = true;
        }
    }

    public void cancelBackgroundTask() {
        if (isTaskExecuting) {
            mBackgroundTask.cancel(true);
            isTaskExecuting = false;
        }
    }

    public void updateExecutingStatus(boolean isExecuting) {
        this.isTaskExecuting = isExecuting;
    }

    /**
     * Called when the fragment is no longer attached to its activity. This is called after onDestroy().
     */
    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        Log.e("..", "Detach");
    }

    /**
     * Async task that checks network connectivity and handles progressbar
     */
    class MyTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected String doInBackground(Integer... params) {
            network_connection = checkNetwork.getNetworkConnectionStatus();
            int x = 10;
            while (x <= 100) {
                try {
                    publishProgress(x);
                    Thread.sleep(1000);
                    if (network_connection) {
                        x = x + 30;
                    } else {
                        x = x + 10;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return "Task Completed.";
        }

        @Override
        protected void onPostExecute(String result) {
            //mStatusCallback.onPostExecute(network_connection);
            if (network_connection) {
                connectionUpdate.setText("Connection is available.");
                ((MainActivity) getActivity()).loadMainFragment(true);

            } else {
                connectionUpdate.setText("No Connection.");
                ((MainActivity) getActivity()).showNoConnection(getString(R.string.title_noconnection), getString(R.string.message_noconnection));
            }
            updateExecutingStatus(false);


        }

        @Override
        protected void onPreExecute() {
            //mStatusCallback.onPreExecute();
            connectionUpdate.setText("Checking connection...");

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar.setProgress(values[0]);

        }
    }


}
