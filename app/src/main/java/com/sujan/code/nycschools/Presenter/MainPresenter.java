package com.sujan.code.nycschools.Presenter;

import com.sujan.code.nycschools.Model.DaggerComponents.MyApplicationDaggerBuild;
import com.sujan.code.nycschools.Model.RetrofitComponents.NYCOpenDataParser;

import javax.inject.Inject;

/**
 * MainPresenter is the Presenter class that is responsible to communicate between UI and model classes .
 */

public class MainPresenter {

    //Injecting the Dagger values
    @Inject
    NYCOpenDataParser NYCOpenDataParser;

    public MainPresenter() {

    }

    public void buildDagger() {
        // Injecting the Dagger Component so that the Dagger provided values can be used.
        MyApplicationDaggerBuild.getMyApplicationDaggerBuild().getMyApplicationDaggerComponent().inject(this);

    }


    /**
     * Called after receiving device's location.
     */
    public void makeOpenNotifyAPIcall() {
        NYCOpenDataParser.getJSONDataFromAPI();
    }

    public void requestDetailsAPIcall(String dbn) {
        NYCOpenDataParser.getSchoolDetailsFromAPI(dbn);
    }


}
