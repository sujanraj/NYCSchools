package com.sujan.code.nycschools.Model.RetrofitComponents.POJOs;

/**
 * Created by macbookpro on 3/23/18.
 */

public class SchoolsData {

    /**
     * academicopportunities1 : Free college courses at neighboring universities
     * academicopportunities2 : International Travel, Special Arts Programs, Music, Internships, College Mentoring English Language Learner Programs: English as a New Language
     * admissionspriority11 : Priority to continuing 8th graders
     * admissionspriority21 : Then to Manhattan students or residents
     * admissionspriority31 : Then to New York City residents
     * attendance_rate : 0.970000029
     * bbl : 1008420034
     * bin : 1089902
     * boro : M
     * borough : MANHATTAN
     * building_code : M868
     * bus : BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9
     * census_tract : 52
     * city : Manhattan
     * code1 : M64A
     * community_board : 5
     * council_district : 2
     * dbn : 02M260
     * directions1 : See theclintonschool.net for more information.
     * ell_programs : English as a New Language
     * extracurricular_activities : CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee
     * fax_number : 212-524-4365
     * finalgrades : 6-12
     * grade9geapplicants1 : 1515
     * grade9geapplicantsperseat1 : 19
     * grade9gefilledflag1 : Y
     * grade9swdapplicants1 : 138
     * grade9swdapplicantsperseat1 : 9
     * grade9swdfilledflag1 : Y
     * grades2018 : 6-11
     * interest1 : Humanities & Interdisciplinary
     * latitude : 40.73653
     * location : 10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)
     * longitude : -73.9927
     * method1 : Screened
     * neighborhood : Chelsea-Union Sq
     * nta : Hudson Yards-Chelsea-Flatiron-Union Square
     * offer_rate1 : Â—57% of offers went to this group
     * overview_paragraph : Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.
     * pct_stu_enough_variety : 0.899999976
     * pct_stu_safe : 0.970000029
     * phone_number : 212-524-4360
     * primary_address_line_1 : 10 East 15th Street
     * program1 : M.S. 260 Clinton School Writers & Artists
     * requirement1_1 : Course Grades: English (87-100), Math (83-100), Social Studies (90-100), Science (88-100)
     * requirement2_1 : Standardized Test Scores: English Language Arts (2.8-4.5), Math (2.8-4.5)
     * requirement3_1 : Attendance and Punctuality
     * requirement4_1 : Writing Exercise
     * requirement5_1 : Group Interview (On-Site)
     * school_10th_seats : 1
     * school_accessibility_description : 1
     * school_email : admissions@theclintonschool.net
     * school_name : Clinton School Writers & Artists, M.S. 260
     * school_sports : Cross Country, Track and Field, Soccer, Flag Football, Basketball
     * seats101 : YesÂ–9
     * seats9ge1 : 80
     * seats9swd1 : 16
     * state_code : NY
     * subway : 1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St
     * total_students : 376
     * website : www.theclintonschool.net
     * zip : 10003
     * academicopportunities3 : The Learning to Work (LTW) partner for Liberation Diploma Plus High School is CAMBA.
     * addtl_info1 : The Learning to Work (LTW) program assists students in overcoming obstacles that impede their progress toward a high school diploma. LTW offers academic and student support, career and educational exploration, work preparation, skills development, and internships that prepare students for rewarding employment and educational experiences after graduation. These LTW supports are provided by a community-based organization (CBO) partner.
     * eligibility1 : For Current 8th Grade Students Â– Open only to students who are at least 15 1/2 years of age and entering high school for the first time. For Other Students Â– Open only to students who are at least 16 years of age and have attended another high school for at least one year
     * language_classes : French, Spanish
     * transfer : 1
     * academicopportunities4 : PEARLS Awards, Academy Awards, Rose Ceremony/Parent Daughter Breakfast, Ice Cream Social.
     * academicopportunities5 : Health and Wellness Program
     * college_career_rate : 0.486000001
     * diplomaendorsements : Science
     * end_time : 2:45pm
     * girls : 1
     * graduation_rate : 0.612999976
     * psal_sports_boys : Baseball, Basketball, Cross Country, Fencing
     * psal_sports_coed : Stunt
     * psal_sports_girls : Basketball, Cross Country, Indoor Track, Outdoor Track, Softball, Volleyball
     * shared_space : Yes
     * start_time : 8:20am
     * advancedplacement_courses : AP English, AP Environmental Science, AP US History
     * campus_name : Prospect Heights Educational Campus
     * prgdesc1 : Provides arts instruction to all students and integrates the arts into the core curriculum.
     * admissionspriority41 : Then to New York City residents
     * international : 1
     * pbat : 1
     * code2 : X95B
     * eligibility2 : Open only to continuing 8th graders
     * grade9geapplicants2 : N/A
     * grade9geapplicantsperseat2 : N/A
     * grade9gefilledflag2 : N/A
     * grade9swdapplicants2 : N/A
     * grade9swdapplicantsperseat2 : N/A
     * grade9swdfilledflag2 : N/A
     * interest2 : Law & Government
     * method2 : For Continuing 8th Graders
     * program2 : Bronx School for Law, Government and Justice for Current Students
     * seats102 : No
     * seats9ge2 : N/A
     * seats9swd2 : N/A
     * admissionspriority12 : Priority to Queens students or residents
     * admissionspriority13 : Priority to students who apply and live in the zoned area
     * admissionspriority22 : Then to New York City residents
     * code3 : Q35Z
     * grade9geapplicants3 : N/A
     * grade9geapplicantsperseat3 : N/A
     * grade9gefilledflag3 : N/A
     * grade9swdapplicants3 : N/A
     * grade9swdapplicantsperseat3 : N/A
     * grade9swdfilledflag3 : N/A
     * interest3 : Zoned
     * method3 : Zoned Priority
     * offer_rate2 : Â—91% of offers went to this group
     * prgdesc2 : This hybrid-program allows students to study key principles of the legal field and the scientific aspects of forensics. Students engage in learning the scientific technology and lab skills related to the field of Medical Forensics and how this scientific evidence can be utilized by the court system to reinforce the foundations of the American legal system. The curriculum also emphasizes scientific, mathematical, technological, and legal research.
     * prgdesc3 : Upon the completion of the Ninth Grade Academy, students can explore career pathways in one of the following areas: Law, Business and Finance, Forensics and Health Sciences, and Engineering.
     * program3 : Zoned
     * requirement1_2 : Course Grades: Math (55-100), Science (55-100)
     * requirement2_2 : Standardized Test Scores: Math (1.7-4.5)
     * requirement3_2 : Attendance
     * seats103 : Yes
     * seats9ge3 : N/A
     * seats9swd3 : N/A
     * admissionspriority32 : Then to New York City residents
     * admissionspriority14 : Open to New York City residents
     * admissionspriority15 : Open to New York City residents
     * admissionspriority16 : Open to New York City residents
     * auditioninformation1 : During the audition, students will submit for review 10 original drawings or photographs on 18Â” x 24Â” poster board that tells a sequential story without words. Images should demonstrate the applicantÂ’s knowledge of visual composition and storytelling. Students will be shown four images and asked to develop an original story that could be the basis for a film. Three questions should be considered while writing: Who are the characters? What problems do they face? What do they do to resolve them?
     * auditioninformation2 : Present a portfolio with 10-15 pieces of original work created from observation and from studentÂ’s own imagination, with a diversity of subject matter and use of media, including examples of line, value and color. Student work should be unframed and need not be matted. Three-dimensional work can be shown in photographs. You will also be required to draw from observation and memory, using pencil.
     * auditioninformation3 : Perform at least one solo selection and three major scales. Take a sight-reading test and a rhythmic comprehension test in which you are required to tap back rhythmic patterns. Bring your own instrument (except piano, tuba, double bass, percussion instruments, guitar amplifiers); bring 2 copies of audition music.
     * auditioninformation4 : Perform the Â“Star Spangled BannerÂ” (accompaniment will be provided) and one minute of a classical or a standard musical theatre piece. Bring sheet music in your key.
     * auditioninformation5 : Students will take an abbreviated ballet class followed by modern and jazz dance combinations. Participants are to audition in appropriate dance attire: leotard, tights and ballet shoes.
     * auditioninformation6 : Present two memorized, contrasting, one-minute monologues that must be from published plays Â– no Internet or original material will be accepted; do a cold reading from selected scenes.
     * code4 : Q40L
     * code5 : Q40M
     * code6 : Q40N
     * grade9geapplicants4 : 550
     * grade9geapplicants5 : 468
     * grade9geapplicants6 : 448
     * grade9geapplicantsperseat4 : 22
     * grade9geapplicantsperseat5 : 20
     * grade9geapplicantsperseat6 : 19
     * grade9gefilledflag4 : Y
     * grade9gefilledflag5 : Y
     * grade9gefilledflag6 : Y
     * grade9swdapplicants4 : 31
     * grade9swdapplicants5 : 27
     * grade9swdapplicants6 : 19
     * grade9swdapplicantsperseat4 : 4
     * grade9swdapplicantsperseat5 : 14
     * grade9swdapplicantsperseat6 : 14
     * grade9swdfilledflag4 : N
     * grade9swdfilledflag5 : Y
     * grade9swdfilledflag6 : Y
     * interest4 : Performing Arts
     * interest5 : Performing Arts
     * interest6 : Performing Arts
     * method4 : Audition
     * method5 : Audition
     * method6 : Audition
     * prgdesc4 : Students perform in the Concert Choir, Chorale, Chamber Singers, musical theatre, Great American Songbook ensemble, opera workshop and participate in cabaret; curriculum includes voice training, music theory, keyboard, sight singing, diction, and audition preparation.
     * prgdesc5 : Ballet, modern dance, dance history, choreography, and dance criticism courses; Jazz, tap, ballroom dancing, kinesiology, anatomy, career management, and dance production classes; students attend live performances and take part in school productions.
     * prgdesc6 : A four-year course of study that includes acting technique, scene study, speech for actors, dramatic writing, and course work in performing Shakespeare. The Drama Studio creates mainstage productions each year.
     * program4 : Vocal Music
     * program5 : Dance
     * program6 : Drama
     * requirement1_3 : Course Grades: English (75-100), Math (70-100), Social Studies (78-100), Science (80-100)
     * requirement1_4 : Course Grades: English (77-100), Math (80-100), Social Studies (70-100), Science (80-100)
     * requirement1_5 : Course Grades: English (80-100), Math (78-100), Social Studies (75-100), Science (75-100)
     * requirement1_6 : Course Grades: English (86-100), Math (81-100), Social Studies (83-100), Science (81-100)
     * requirement2_3 : Standardized Test Scores: English Language Arts (2.0-4.5), Math (2.4-4.5)
     * requirement2_4 : Standardized Test Scores: English Language Arts (2.2-4.5), Math (2.6-4.5)
     * requirement2_5 : Standardized Test Scores: English Language Arts (2.1-4.5), Math (1.9-4.5)
     * requirement2_6 : Standardized Test Scores: English Language Arts (2.8-4.5), Math (2.8-4.5)
     * requirement3_3 : Attendance and Punctuality
     * requirement3_4 : Attendance and Punctuality
     * requirement3_5 : Attendance and Punctuality
     * requirement3_6 : Attendance and Punctuality
     * requirement4_2 : Audition
     * requirement4_3 : Audition
     * requirement4_4 : Audition
     * requirement4_5 : Audition
     * requirement4_6 : Audition
     * seats104 : Yes-2
     * seats105 : Yes-5
     * seats106 : Yes-5
     * seats9ge4 : 25
     * seats9ge5 : 23
     * seats9ge6 : 23
     * seats9swd4 : 7
     * seats9swd5 : 2
     * seats9swd6 : 2
     * earlycollege : 1
     * admissionspriority51 : Then to Brooklyn students or residents
     * admissionspriority61 : Then to New York City residents
     * admissionspriority23 : Then to New York City residents
     * admissionspriority24 : Then to New York City residents
     * offer_rate3 : Â—9% of offers went to this group
     * offer_rate4 : Â—6% of offers went to this group
     * ptech : 1
     * admissionspriority17 : Guaranteed offer to students who apply and live in the zoned area
     * admissionspriority25 : Then to New York City residents
     * admissionspriority26 : Then to New York City residents
     * admissionspriority34 : Then to Bronx students or residents
     * admissionspriority44 : Then to New York City residents
     * code7 : X25Z
     * grade9geapplicants7 : N/A
     * grade9geapplicantsperseat7 : N/A
     * grade9gefilledflag7 : N/A
     * grade9swdapplicants7 : N/A
     * grade9swdapplicantsperseat7 : N/A
     * grade9swdfilledflag7 : N/A
     * interest7 : Zoned
     * method7 : Zoned Guarantee
     * offer_rate5 : Â—98% of offers went to this group
     * offer_rate6 : Â—100% of offers went to this group
     * program7 : Zoned
     * seats107 : Yes
     * seats9ge7 : N/A
     * seats9swd7 : N/A
     * boys : 1
     * directions2 : Students are directly contacted by the school and will receive notification by mail and or by phone. It is important that the latest contact information listed for all applicants is correct so that we can contact you.
     * requirement5_2 : Writing Sample
     * admissionspriority18 : Guaranteed offer to students who apply and live in the zoned area
     * code8 : Q24Z
     * directions7 : Please Contact the School About the On-Site Requirement.
     * grade9geapplicants8 : N/A
     * grade9geapplicantsperseat8 : N/A
     * grade9gefilledflag8 : N/A
     * grade9swdapplicants8 : N/A
     * grade9swdapplicantsperseat8 : N/A
     * grade9swdfilledflag8 : N/A
     * interest8 : Zoned
     * method8 : Zoned Guarantee
     * prgdesc7 : Biz-tek prepares students for college and careers in a range of business and STEM fields. CTE endorsed programs in Virtual Enterprise, Software Engineering; CAD/CAM-Computer Aided Design/Computer Aided Manufacturing and Information Technology & Computer Networking; training for Cisco certifications, CompTIAA+, Cisco Certified Entry Networking Technician (C-CENT) and Cisco Certified Networking Associate (CCNA).
     * program8 : Zoned
     * requirement1_7 : Test (on-site)
     * seats108 : Yes
     * seats9ge8 : N/A
     * seats9swd8 : N/A
     * common_audition1 : 1
     * geoeligibility : Open only to Bronx students/residents
     * applicants1specialized : 16592
     * appperseat1specialized : 160
     * seats1specialized : 104
     * specialized : 1
     * admissionspriority42 : Then to New York City residents
     * admissionspriority52 : Then to New York City residents
     * common_audition4 : 1
     * common_audition5 : 1
     * requirement5_3 : Problem-Solving Exercise
     * requirement6_3 : Writing Exercise
     * requirement2_7 : Standardized Test Scores: English Language Arts (1.8-4.5), Math (1.6-4.5)
     * requirement3_7 : Attendance and Punctuality
     * admissionspriority71 : Then to New York City residents
     * eligibility3 : Open only to Queens students or residents
     * admissionspriority35 : Then to New York City residents
     * requirement6_2 : Writing Exercise
     * requirement5_4 : Writing Sample
     * common_audition2 : 1
     * admissionspriority33 : Then to New York City residents
     * directions3 : Students can schedule a school tour or interview during Open House dates. Speak with your guidance counselor to make an appointment.
     * directions4 : Students can schedule a school tour or interview during Open House dates. Speak with your guidance counselor to make an appointment.
     * admissionspriority36 : Then to New York City residents who attend an information session
     * admissionspriority46 : Then to Districts 3, 5 and 6 students or residents
     * admissionspriority56 : Then to New York City residents
     * common_audition3 : 1
     * admissionspriority27 : Then to students who have been in a Transitional Bilingual Education Spanish middle school program
     * admissionspriority37 : Then to New York City residents
     * prgdesc8 : .
     * admissionspriority54 : Then to District 14 students or residents
     * admissionspriority64 : Then to Brooklyn students or residents
     * admissionspriority74 : Then to New York City residents
     * requirement6_1 : Portfolio of student work
     * eligibility4 : Open only to students whose home language is Haitian Creole
     * auditioninformation7 : Perform two contrasting monologues (one minute each). Perform an on-demand dramatic or movement activity (e.g. impromptu reading from provided script or improvisation). Wear attire that allows free movement.
     * common_audition6 : 1
     * common_audition7 : 1
     * eligibility5 : Open only to Brooklyn students or residents
     * eligibility6 : Open only to Brooklyn students or residents
     * eligibility7 : Open only to Brooklyn students or residents
     * requirement4_7 : Audition
     * requirement5_5 : Interview
     * admissionspriority110 : Guaranteed offer to students who apply and live in the zoned area
     * admissionspriority19 : Priority to Staten Island students or residents
     * admissionspriority28 : Then to New York City residents
     * admissionspriority29 : Then to New York City residents
     * code10 : R19Z
     * code9 : R19J
     * grade9geapplicants10 : N/A
     * grade9geapplicants9 : 176
     * grade9geapplicantsperseat10 : N/A
     * grade9geapplicantsperseat9 : 3
     * grade9gefilledflag10 : N/A
     * grade9gefilledflag9 : N
     * grade9swdapplicants10 : N/A
     * grade9swdapplicants9 : 63
     * grade9swdapplicantsperseat10 : N/A
     * grade9swdapplicantsperseat9 : 5
     * grade9swdfilledflag10 : N/A
     * grade9swdfilledflag9 : Y
     * interest10 : Zoned
     * interest9 : Visual Art & Design
     * method10 : Zoned Guarantee
     * method9 : Unscreened
     * offer_rate7 : Â—93% of offers went to this group
     * offer_rate8 : Â—100% of offers went to this group
     * offer_rate9 : Â—96% of offers went to this group
     * prgdesc10 : Comprehensive Academic Program
     * prgdesc9 : The Visual Arts program offers students the opportunity to study fine arts or graphic design through a variety of classes that are enriched by after-school programs and a design partnership with Roundabout Theater. Course offerings include: Drawing, Painting, AP Art History, IB Art, and/or CTE Graphic Design using Adobe Creative Suite.
     * program10 : Zoned
     * program9 : Visual Arts
     * requirement1_8 : Course Grades: English (60-100), Math (65-100), Social Studies (65-100), Science (70-100)
     * requirement2_8 : Standardized Test Scores: English Language Arts (1.9-4.5), Math (1.9-4.5)
     * requirement3_8 : Attendance and Punctuality
     * seats1010 : Yes
     * seats109 : No
     * seats9ge10 : N/A
     * seats9ge9 : 55
     * seats9swd10 : N/A
     * seats9swd9 : 13
     * admissionspriority43 : Then to New York City residents
     * applicants2specialized : 1541
     * applicants3specialized : 3067
     * applicants4specialized : 536
     * applicants5specialized : 2616
     * applicants6specialized : 2429
     * appperseat2specialized : 13
     * appperseat3specialized : 17
     * appperseat4specialized : 19
     * appperseat5specialized : 26
     * appperseat6specialized : 12
     * directions5 : Academic information will be considered for students who successfully auditioned
     * directions6 : Academic information will be considered for students who successfully auditioned
     * requirement5_6 : Audition
     * seats2specialized : 118
     * seats3specialized : 181
     * seats4specialized : 28
     * seats5specialized : 99
     * seats6specialized : 211
     * admissionspriority53 : Then to Brooklyn students or residents
     * admissionspriority62 : Then to New York City residents
     * admissionspriority63 : Then to New York City residents
     * requirement5_7 : Honors/Accelerated Coursework
     * requirement6_7 : Zoned to School
     */

    private String academicopportunities1;
    private String academicopportunities2;
    private String admissionspriority11;
    private String admissionspriority21;
    private String admissionspriority31;
    private String attendance_rate;
    private String bbl;
    private String bin;
    private String boro;
    private String borough;
    private String building_code;
    private String bus;
    private String census_tract;
    private String city;
    private String code1;
    private String community_board;
    private String council_district;
    private String dbn;
    private String directions1;
    private String ell_programs;
    private String extracurricular_activities;
    private String fax_number;
    private String finalgrades;
    private String grade9geapplicants1;
    private String grade9geapplicantsperseat1;
    private String grade9gefilledflag1;
    private String grade9swdapplicants1;
    private String grade9swdapplicantsperseat1;
    private String grade9swdfilledflag1;
    private String grades2018;
    private String interest1;
    private String latitude;
    private String location;
    private String longitude;
    private String method1;
    private String neighborhood;
    private String nta;
    private String offer_rate1;
    private String overview_paragraph;
    private String pct_stu_enough_variety;
    private String pct_stu_safe;
    private String phone_number;
    private String primary_address_line_1;
    private String program1;
    private String requirement1_1;
    private String requirement2_1;
    private String requirement3_1;
    private String requirement4_1;
    private String requirement5_1;
    private String school_10th_seats;
    private String school_accessibility_description;
    private String school_email;
    private String school_name;
    private String school_sports;
    private String seats101;
    private String seats9ge1;
    private String seats9swd1;
    private String state_code;
    private String subway;
    private String total_students;
    private String website;
    private String zip;
    private String academicopportunities3;
    private String addtl_info1;
    private String eligibility1;
    private String language_classes;
    private String transfer;
    private String academicopportunities4;
    private String academicopportunities5;
    private String college_career_rate;
    private String diplomaendorsements;
    private String end_time;
    private String girls;
    private String graduation_rate;
    private String psal_sports_boys;
    private String psal_sports_coed;
    private String psal_sports_girls;
    private String shared_space;
    private String start_time;
    private String advancedplacement_courses;
    private String campus_name;
    private String prgdesc1;
    private String admissionspriority41;
    private String international;
    private String pbat;
    private String code2;
    private String eligibility2;
    private String grade9geapplicants2;
    private String grade9geapplicantsperseat2;
    private String grade9gefilledflag2;
    private String grade9swdapplicants2;
    private String grade9swdapplicantsperseat2;
    private String grade9swdfilledflag2;
    private String interest2;
    private String method2;
    private String program2;
    private String seats102;
    private String seats9ge2;
    private String seats9swd2;
    private String admissionspriority12;
    private String admissionspriority13;
    private String admissionspriority22;
    private String code3;
    private String grade9geapplicants3;
    private String grade9geapplicantsperseat3;
    private String grade9gefilledflag3;
    private String grade9swdapplicants3;
    private String grade9swdapplicantsperseat3;
    private String grade9swdfilledflag3;
    private String interest3;
    private String method3;
    private String offer_rate2;
    private String prgdesc2;
    private String prgdesc3;
    private String program3;
    private String requirement1_2;
    private String requirement2_2;
    private String requirement3_2;
    private String seats103;
    private String seats9ge3;
    private String seats9swd3;
    private String admissionspriority32;
    private String admissionspriority14;
    private String admissionspriority15;
    private String admissionspriority16;
    private String auditioninformation1;
    private String auditioninformation2;
    private String auditioninformation3;
    private String auditioninformation4;
    private String auditioninformation5;
    private String auditioninformation6;
    private String code4;
    private String code5;
    private String code6;
    private String grade9geapplicants4;
    private String grade9geapplicants5;
    private String grade9geapplicants6;
    private String grade9geapplicantsperseat4;
    private String grade9geapplicantsperseat5;
    private String grade9geapplicantsperseat6;
    private String grade9gefilledflag4;
    private String grade9gefilledflag5;
    private String grade9gefilledflag6;
    private String grade9swdapplicants4;
    private String grade9swdapplicants5;
    private String grade9swdapplicants6;
    private String grade9swdapplicantsperseat4;
    private String grade9swdapplicantsperseat5;
    private String grade9swdapplicantsperseat6;
    private String grade9swdfilledflag4;
    private String grade9swdfilledflag5;
    private String grade9swdfilledflag6;
    private String interest4;
    private String interest5;
    private String interest6;
    private String method4;
    private String method5;
    private String method6;
    private String prgdesc4;
    private String prgdesc5;
    private String prgdesc6;
    private String program4;
    private String program5;
    private String program6;
    private String requirement1_3;
    private String requirement1_4;
    private String requirement1_5;
    private String requirement1_6;
    private String requirement2_3;
    private String requirement2_4;
    private String requirement2_5;
    private String requirement2_6;
    private String requirement3_3;
    private String requirement3_4;
    private String requirement3_5;
    private String requirement3_6;
    private String requirement4_2;
    private String requirement4_3;
    private String requirement4_4;
    private String requirement4_5;
    private String requirement4_6;
    private String seats104;
    private String seats105;
    private String seats106;
    private String seats9ge4;
    private String seats9ge5;
    private String seats9ge6;
    private String seats9swd4;
    private String seats9swd5;
    private String seats9swd6;
    private String earlycollege;
    private String admissionspriority51;
    private String admissionspriority61;
    private String admissionspriority23;
    private String admissionspriority24;
    private String offer_rate3;
    private String offer_rate4;
    private String ptech;
    private String admissionspriority17;
    private String admissionspriority25;
    private String admissionspriority26;
    private String admissionspriority34;
    private String admissionspriority44;
    private String code7;
    private String grade9geapplicants7;
    private String grade9geapplicantsperseat7;
    private String grade9gefilledflag7;
    private String grade9swdapplicants7;
    private String grade9swdapplicantsperseat7;
    private String grade9swdfilledflag7;
    private String interest7;
    private String method7;
    private String offer_rate5;
    private String offer_rate6;
    private String program7;
    private String seats107;
    private String seats9ge7;
    private String seats9swd7;
    private String boys;
    private String directions2;
    private String requirement5_2;
    private String admissionspriority18;
    private String code8;
    private String directions7;
    private String grade9geapplicants8;
    private String grade9geapplicantsperseat8;
    private String grade9gefilledflag8;
    private String grade9swdapplicants8;
    private String grade9swdapplicantsperseat8;
    private String grade9swdfilledflag8;
    private String interest8;
    private String method8;
    private String prgdesc7;
    private String program8;
    private String requirement1_7;
    private String seats108;
    private String seats9ge8;
    private String seats9swd8;
    private String common_audition1;
    private String geoeligibility;
    private String applicants1specialized;
    private String appperseat1specialized;
    private String seats1specialized;
    private String specialized;
    private String admissionspriority42;
    private String admissionspriority52;
    private String common_audition4;
    private String common_audition5;
    private String requirement5_3;
    private String requirement6_3;
    private String requirement2_7;
    private String requirement3_7;
    private String admissionspriority71;
    private String eligibility3;
    private String admissionspriority35;
    private String requirement6_2;
    private String requirement5_4;
    private String common_audition2;
    private String admissionspriority33;
    private String directions3;
    private String directions4;
    private String admissionspriority36;
    private String admissionspriority46;
    private String admissionspriority56;
    private String common_audition3;
    private String admissionspriority27;
    private String admissionspriority37;
    private String prgdesc8;
    private String admissionspriority54;
    private String admissionspriority64;
    private String admissionspriority74;
    private String requirement6_1;
    private String eligibility4;
    private String auditioninformation7;
    private String common_audition6;
    private String common_audition7;
    private String eligibility5;
    private String eligibility6;
    private String eligibility7;
    private String requirement4_7;
    private String requirement5_5;
    private String admissionspriority110;
    private String admissionspriority19;
    private String admissionspriority28;
    private String admissionspriority29;
    private String code10;
    private String code9;
    private String grade9geapplicants10;
    private String grade9geapplicants9;
    private String grade9geapplicantsperseat10;
    private String grade9geapplicantsperseat9;
    private String grade9gefilledflag10;
    private String grade9gefilledflag9;
    private String grade9swdapplicants10;
    private String grade9swdapplicants9;
    private String grade9swdapplicantsperseat10;
    private String grade9swdapplicantsperseat9;
    private String grade9swdfilledflag10;
    private String grade9swdfilledflag9;
    private String interest10;
    private String interest9;
    private String method10;
    private String method9;
    private String offer_rate7;
    private String offer_rate8;
    private String offer_rate9;
    private String prgdesc10;
    private String prgdesc9;
    private String program10;
    private String program9;
    private String requirement1_8;
    private String requirement2_8;
    private String requirement3_8;
    private String seats1010;
    private String seats109;
    private String seats9ge10;
    private String seats9ge9;
    private String seats9swd10;
    private String seats9swd9;
    private String admissionspriority43;
    private String applicants2specialized;
    private String applicants3specialized;
    private String applicants4specialized;
    private String applicants5specialized;
    private String applicants6specialized;
    private String appperseat2specialized;
    private String appperseat3specialized;
    private String appperseat4specialized;
    private String appperseat5specialized;
    private String appperseat6specialized;
    private String directions5;
    private String directions6;
    private String requirement5_6;
    private String seats2specialized;
    private String seats3specialized;
    private String seats4specialized;
    private String seats5specialized;
    private String seats6specialized;
    private String admissionspriority53;
    private String admissionspriority62;
    private String admissionspriority63;
    private String requirement5_7;
    private String requirement6_7;

    public String getAcademicopportunities1() {
        return academicopportunities1;
    }

    public void setAcademicopportunities1(String academicopportunities1) {
        this.academicopportunities1 = academicopportunities1;
    }

    public String getAcademicopportunities2() {
        return academicopportunities2;
    }

    public void setAcademicopportunities2(String academicopportunities2) {
        this.academicopportunities2 = academicopportunities2;
    }

    public String getAdmissionspriority11() {
        return admissionspriority11;
    }

    public void setAdmissionspriority11(String admissionspriority11) {
        this.admissionspriority11 = admissionspriority11;
    }

    public String getAdmissionspriority21() {
        return admissionspriority21;
    }

    public void setAdmissionspriority21(String admissionspriority21) {
        this.admissionspriority21 = admissionspriority21;
    }

    public String getAdmissionspriority31() {
        return admissionspriority31;
    }

    public void setAdmissionspriority31(String admissionspriority31) {
        this.admissionspriority31 = admissionspriority31;
    }

    public String getAttendance_rate() {
        return attendance_rate;
    }

    public void setAttendance_rate(String attendance_rate) {
        this.attendance_rate = attendance_rate;
    }

    public String getBbl() {
        return bbl;
    }

    public void setBbl(String bbl) {
        this.bbl = bbl;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getBoro() {
        return boro;
    }

    public void setBoro(String boro) {
        this.boro = boro;
    }

    public String getBorough() {
        return borough;
    }

    public void setBorough(String borough) {
        this.borough = borough;
    }

    public String getBuilding_code() {
        return building_code;
    }

    public void setBuilding_code(String building_code) {
        this.building_code = building_code;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getCensus_tract() {
        return census_tract;
    }

    public void setCensus_tract(String census_tract) {
        this.census_tract = census_tract;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCode1() {
        return code1;
    }

    public void setCode1(String code1) {
        this.code1 = code1;
    }

    public String getCommunity_board() {
        return community_board;
    }

    public void setCommunity_board(String community_board) {
        this.community_board = community_board;
    }

    public String getCouncil_district() {
        return council_district;
    }

    public void setCouncil_district(String council_district) {
        this.council_district = council_district;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getDirections1() {
        return directions1;
    }

    public void setDirections1(String directions1) {
        this.directions1 = directions1;
    }

    public String getEll_programs() {
        return ell_programs;
    }

    public void setEll_programs(String ell_programs) {
        this.ell_programs = ell_programs;
    }

    public String getExtracurricular_activities() {
        return extracurricular_activities;
    }

    public void setExtracurricular_activities(String extracurricular_activities) {
        this.extracurricular_activities = extracurricular_activities;
    }

    public String getFax_number() {
        return fax_number;
    }

    public void setFax_number(String fax_number) {
        this.fax_number = fax_number;
    }

    public String getFinalgrades() {
        return finalgrades;
    }

    public void setFinalgrades(String finalgrades) {
        this.finalgrades = finalgrades;
    }

    public String getGrade9geapplicants1() {
        return grade9geapplicants1;
    }

    public void setGrade9geapplicants1(String grade9geapplicants1) {
        this.grade9geapplicants1 = grade9geapplicants1;
    }

    public String getGrade9geapplicantsperseat1() {
        return grade9geapplicantsperseat1;
    }

    public void setGrade9geapplicantsperseat1(String grade9geapplicantsperseat1) {
        this.grade9geapplicantsperseat1 = grade9geapplicantsperseat1;
    }

    public String getGrade9gefilledflag1() {
        return grade9gefilledflag1;
    }

    public void setGrade9gefilledflag1(String grade9gefilledflag1) {
        this.grade9gefilledflag1 = grade9gefilledflag1;
    }

    public String getGrade9swdapplicants1() {
        return grade9swdapplicants1;
    }

    public void setGrade9swdapplicants1(String grade9swdapplicants1) {
        this.grade9swdapplicants1 = grade9swdapplicants1;
    }

    public String getGrade9swdapplicantsperseat1() {
        return grade9swdapplicantsperseat1;
    }

    public void setGrade9swdapplicantsperseat1(String grade9swdapplicantsperseat1) {
        this.grade9swdapplicantsperseat1 = grade9swdapplicantsperseat1;
    }

    public String getGrade9swdfilledflag1() {
        return grade9swdfilledflag1;
    }

    public void setGrade9swdfilledflag1(String grade9swdfilledflag1) {
        this.grade9swdfilledflag1 = grade9swdfilledflag1;
    }

    public String getGrades2018() {
        return grades2018;
    }

    public void setGrades2018(String grades2018) {
        this.grades2018 = grades2018;
    }

    public String getInterest1() {
        return interest1;
    }

    public void setInterest1(String interest1) {
        this.interest1 = interest1;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMethod1() {
        return method1;
    }

    public void setMethod1(String method1) {
        this.method1 = method1;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getNta() {
        return nta;
    }

    public void setNta(String nta) {
        this.nta = nta;
    }

    public String getOffer_rate1() {
        return offer_rate1;
    }

    public void setOffer_rate1(String offer_rate1) {
        this.offer_rate1 = offer_rate1;
    }

    public String getOverview_paragraph() {
        return overview_paragraph;
    }

    public void setOverview_paragraph(String overview_paragraph) {
        this.overview_paragraph = overview_paragraph;
    }

    public String getPct_stu_enough_variety() {
        return pct_stu_enough_variety;
    }

    public void setPct_stu_enough_variety(String pct_stu_enough_variety) {
        this.pct_stu_enough_variety = pct_stu_enough_variety;
    }

    public String getPct_stu_safe() {
        return pct_stu_safe;
    }

    public void setPct_stu_safe(String pct_stu_safe) {
        this.pct_stu_safe = pct_stu_safe;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getPrimary_address_line_1() {
        return primary_address_line_1;
    }

    public void setPrimary_address_line_1(String primary_address_line_1) {
        this.primary_address_line_1 = primary_address_line_1;
    }

    public String getProgram1() {
        return program1;
    }

    public void setProgram1(String program1) {
        this.program1 = program1;
    }

    public String getRequirement1_1() {
        return requirement1_1;
    }

    public void setRequirement1_1(String requirement1_1) {
        this.requirement1_1 = requirement1_1;
    }

    public String getRequirement2_1() {
        return requirement2_1;
    }

    public void setRequirement2_1(String requirement2_1) {
        this.requirement2_1 = requirement2_1;
    }

    public String getRequirement3_1() {
        return requirement3_1;
    }

    public void setRequirement3_1(String requirement3_1) {
        this.requirement3_1 = requirement3_1;
    }

    public String getRequirement4_1() {
        return requirement4_1;
    }

    public void setRequirement4_1(String requirement4_1) {
        this.requirement4_1 = requirement4_1;
    }

    public String getRequirement5_1() {
        return requirement5_1;
    }

    public void setRequirement5_1(String requirement5_1) {
        this.requirement5_1 = requirement5_1;
    }

    public String getSchool_10th_seats() {
        return school_10th_seats;
    }

    public void setSchool_10th_seats(String school_10th_seats) {
        this.school_10th_seats = school_10th_seats;
    }

    public String getSchool_accessibility_description() {
        return school_accessibility_description;
    }

    public void setSchool_accessibility_description(String school_accessibility_description) {
        this.school_accessibility_description = school_accessibility_description;
    }

    public String getSchool_email() {
        return school_email;
    }

    public void setSchool_email(String school_email) {
        this.school_email = school_email;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getSchool_sports() {
        return school_sports;
    }

    public void setSchool_sports(String school_sports) {
        this.school_sports = school_sports;
    }

    public String getSeats101() {
        return seats101;
    }

    public void setSeats101(String seats101) {
        this.seats101 = seats101;
    }

    public String getSeats9ge1() {
        return seats9ge1;
    }

    public void setSeats9ge1(String seats9ge1) {
        this.seats9ge1 = seats9ge1;
    }

    public String getSeats9swd1() {
        return seats9swd1;
    }

    public void setSeats9swd1(String seats9swd1) {
        this.seats9swd1 = seats9swd1;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getSubway() {
        return subway;
    }

    public void setSubway(String subway) {
        this.subway = subway;
    }

    public String getTotal_students() {
        return total_students;
    }

    public void setTotal_students(String total_students) {
        this.total_students = total_students;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getAcademicopportunities3() {
        return academicopportunities3;
    }

    public void setAcademicopportunities3(String academicopportunities3) {
        this.academicopportunities3 = academicopportunities3;
    }

    public String getAddtl_info1() {
        return addtl_info1;
    }

    public void setAddtl_info1(String addtl_info1) {
        this.addtl_info1 = addtl_info1;
    }

    public String getEligibility1() {
        return eligibility1;
    }

    public void setEligibility1(String eligibility1) {
        this.eligibility1 = eligibility1;
    }

    public String getLanguage_classes() {
        return language_classes;
    }

    public void setLanguage_classes(String language_classes) {
        this.language_classes = language_classes;
    }

    public String getTransfer() {
        return transfer;
    }

    public void setTransfer(String transfer) {
        this.transfer = transfer;
    }

    public String getAcademicopportunities4() {
        return academicopportunities4;
    }

    public void setAcademicopportunities4(String academicopportunities4) {
        this.academicopportunities4 = academicopportunities4;
    }

    public String getAcademicopportunities5() {
        return academicopportunities5;
    }

    public void setAcademicopportunities5(String academicopportunities5) {
        this.academicopportunities5 = academicopportunities5;
    }

    public String getCollege_career_rate() {
        return college_career_rate;
    }

    public void setCollege_career_rate(String college_career_rate) {
        this.college_career_rate = college_career_rate;
    }

    public String getDiplomaendorsements() {
        return diplomaendorsements;
    }

    public void setDiplomaendorsements(String diplomaendorsements) {
        this.diplomaendorsements = diplomaendorsements;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getGirls() {
        return girls;
    }

    public void setGirls(String girls) {
        this.girls = girls;
    }

    public String getGraduation_rate() {
        return graduation_rate;
    }

    public void setGraduation_rate(String graduation_rate) {
        this.graduation_rate = graduation_rate;
    }

    public String getPsal_sports_boys() {
        return psal_sports_boys;
    }

    public void setPsal_sports_boys(String psal_sports_boys) {
        this.psal_sports_boys = psal_sports_boys;
    }

    public String getPsal_sports_coed() {
        return psal_sports_coed;
    }

    public void setPsal_sports_coed(String psal_sports_coed) {
        this.psal_sports_coed = psal_sports_coed;
    }

    public String getPsal_sports_girls() {
        return psal_sports_girls;
    }

    public void setPsal_sports_girls(String psal_sports_girls) {
        this.psal_sports_girls = psal_sports_girls;
    }

    public String getShared_space() {
        return shared_space;
    }

    public void setShared_space(String shared_space) {
        this.shared_space = shared_space;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getAdvancedplacement_courses() {
        return advancedplacement_courses;
    }

    public void setAdvancedplacement_courses(String advancedplacement_courses) {
        this.advancedplacement_courses = advancedplacement_courses;
    }

    public String getCampus_name() {
        return campus_name;
    }

    public void setCampus_name(String campus_name) {
        this.campus_name = campus_name;
    }

    public String getPrgdesc1() {
        return prgdesc1;
    }

    public void setPrgdesc1(String prgdesc1) {
        this.prgdesc1 = prgdesc1;
    }

    public String getAdmissionspriority41() {
        return admissionspriority41;
    }

    public void setAdmissionspriority41(String admissionspriority41) {
        this.admissionspriority41 = admissionspriority41;
    }

    public String getInternational() {
        return international;
    }

    public void setInternational(String international) {
        this.international = international;
    }

    public String getPbat() {
        return pbat;
    }

    public void setPbat(String pbat) {
        this.pbat = pbat;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public String getEligibility2() {
        return eligibility2;
    }

    public void setEligibility2(String eligibility2) {
        this.eligibility2 = eligibility2;
    }

    public String getGrade9geapplicants2() {
        return grade9geapplicants2;
    }

    public void setGrade9geapplicants2(String grade9geapplicants2) {
        this.grade9geapplicants2 = grade9geapplicants2;
    }

    public String getGrade9geapplicantsperseat2() {
        return grade9geapplicantsperseat2;
    }

    public void setGrade9geapplicantsperseat2(String grade9geapplicantsperseat2) {
        this.grade9geapplicantsperseat2 = grade9geapplicantsperseat2;
    }

    public String getGrade9gefilledflag2() {
        return grade9gefilledflag2;
    }

    public void setGrade9gefilledflag2(String grade9gefilledflag2) {
        this.grade9gefilledflag2 = grade9gefilledflag2;
    }

    public String getGrade9swdapplicants2() {
        return grade9swdapplicants2;
    }

    public void setGrade9swdapplicants2(String grade9swdapplicants2) {
        this.grade9swdapplicants2 = grade9swdapplicants2;
    }

    public String getGrade9swdapplicantsperseat2() {
        return grade9swdapplicantsperseat2;
    }

    public void setGrade9swdapplicantsperseat2(String grade9swdapplicantsperseat2) {
        this.grade9swdapplicantsperseat2 = grade9swdapplicantsperseat2;
    }

    public String getGrade9swdfilledflag2() {
        return grade9swdfilledflag2;
    }

    public void setGrade9swdfilledflag2(String grade9swdfilledflag2) {
        this.grade9swdfilledflag2 = grade9swdfilledflag2;
    }

    public String getInterest2() {
        return interest2;
    }

    public void setInterest2(String interest2) {
        this.interest2 = interest2;
    }

    public String getMethod2() {
        return method2;
    }

    public void setMethod2(String method2) {
        this.method2 = method2;
    }

    public String getProgram2() {
        return program2;
    }

    public void setProgram2(String program2) {
        this.program2 = program2;
    }

    public String getSeats102() {
        return seats102;
    }

    public void setSeats102(String seats102) {
        this.seats102 = seats102;
    }

    public String getSeats9ge2() {
        return seats9ge2;
    }

    public void setSeats9ge2(String seats9ge2) {
        this.seats9ge2 = seats9ge2;
    }

    public String getSeats9swd2() {
        return seats9swd2;
    }

    public void setSeats9swd2(String seats9swd2) {
        this.seats9swd2 = seats9swd2;
    }

    public String getAdmissionspriority12() {
        return admissionspriority12;
    }

    public void setAdmissionspriority12(String admissionspriority12) {
        this.admissionspriority12 = admissionspriority12;
    }

    public String getAdmissionspriority13() {
        return admissionspriority13;
    }

    public void setAdmissionspriority13(String admissionspriority13) {
        this.admissionspriority13 = admissionspriority13;
    }

    public String getAdmissionspriority22() {
        return admissionspriority22;
    }

    public void setAdmissionspriority22(String admissionspriority22) {
        this.admissionspriority22 = admissionspriority22;
    }

    public String getCode3() {
        return code3;
    }

    public void setCode3(String code3) {
        this.code3 = code3;
    }

    public String getGrade9geapplicants3() {
        return grade9geapplicants3;
    }

    public void setGrade9geapplicants3(String grade9geapplicants3) {
        this.grade9geapplicants3 = grade9geapplicants3;
    }

    public String getGrade9geapplicantsperseat3() {
        return grade9geapplicantsperseat3;
    }

    public void setGrade9geapplicantsperseat3(String grade9geapplicantsperseat3) {
        this.grade9geapplicantsperseat3 = grade9geapplicantsperseat3;
    }

    public String getGrade9gefilledflag3() {
        return grade9gefilledflag3;
    }

    public void setGrade9gefilledflag3(String grade9gefilledflag3) {
        this.grade9gefilledflag3 = grade9gefilledflag3;
    }

    public String getGrade9swdapplicants3() {
        return grade9swdapplicants3;
    }

    public void setGrade9swdapplicants3(String grade9swdapplicants3) {
        this.grade9swdapplicants3 = grade9swdapplicants3;
    }

    public String getGrade9swdapplicantsperseat3() {
        return grade9swdapplicantsperseat3;
    }

    public void setGrade9swdapplicantsperseat3(String grade9swdapplicantsperseat3) {
        this.grade9swdapplicantsperseat3 = grade9swdapplicantsperseat3;
    }

    public String getGrade9swdfilledflag3() {
        return grade9swdfilledflag3;
    }

    public void setGrade9swdfilledflag3(String grade9swdfilledflag3) {
        this.grade9swdfilledflag3 = grade9swdfilledflag3;
    }

    public String getInterest3() {
        return interest3;
    }

    public void setInterest3(String interest3) {
        this.interest3 = interest3;
    }

    public String getMethod3() {
        return method3;
    }

    public void setMethod3(String method3) {
        this.method3 = method3;
    }

    public String getOffer_rate2() {
        return offer_rate2;
    }

    public void setOffer_rate2(String offer_rate2) {
        this.offer_rate2 = offer_rate2;
    }

    public String getPrgdesc2() {
        return prgdesc2;
    }

    public void setPrgdesc2(String prgdesc2) {
        this.prgdesc2 = prgdesc2;
    }

    public String getPrgdesc3() {
        return prgdesc3;
    }

    public void setPrgdesc3(String prgdesc3) {
        this.prgdesc3 = prgdesc3;
    }

    public String getProgram3() {
        return program3;
    }

    public void setProgram3(String program3) {
        this.program3 = program3;
    }

    public String getRequirement1_2() {
        return requirement1_2;
    }

    public void setRequirement1_2(String requirement1_2) {
        this.requirement1_2 = requirement1_2;
    }

    public String getRequirement2_2() {
        return requirement2_2;
    }

    public void setRequirement2_2(String requirement2_2) {
        this.requirement2_2 = requirement2_2;
    }

    public String getRequirement3_2() {
        return requirement3_2;
    }

    public void setRequirement3_2(String requirement3_2) {
        this.requirement3_2 = requirement3_2;
    }

    public String getSeats103() {
        return seats103;
    }

    public void setSeats103(String seats103) {
        this.seats103 = seats103;
    }

    public String getSeats9ge3() {
        return seats9ge3;
    }

    public void setSeats9ge3(String seats9ge3) {
        this.seats9ge3 = seats9ge3;
    }

    public String getSeats9swd3() {
        return seats9swd3;
    }

    public void setSeats9swd3(String seats9swd3) {
        this.seats9swd3 = seats9swd3;
    }

    public String getAdmissionspriority32() {
        return admissionspriority32;
    }

    public void setAdmissionspriority32(String admissionspriority32) {
        this.admissionspriority32 = admissionspriority32;
    }

    public String getAdmissionspriority14() {
        return admissionspriority14;
    }

    public void setAdmissionspriority14(String admissionspriority14) {
        this.admissionspriority14 = admissionspriority14;
    }

    public String getAdmissionspriority15() {
        return admissionspriority15;
    }

    public void setAdmissionspriority15(String admissionspriority15) {
        this.admissionspriority15 = admissionspriority15;
    }

    public String getAdmissionspriority16() {
        return admissionspriority16;
    }

    public void setAdmissionspriority16(String admissionspriority16) {
        this.admissionspriority16 = admissionspriority16;
    }

    public String getAuditioninformation1() {
        return auditioninformation1;
    }

    public void setAuditioninformation1(String auditioninformation1) {
        this.auditioninformation1 = auditioninformation1;
    }

    public String getAuditioninformation2() {
        return auditioninformation2;
    }

    public void setAuditioninformation2(String auditioninformation2) {
        this.auditioninformation2 = auditioninformation2;
    }

    public String getAuditioninformation3() {
        return auditioninformation3;
    }

    public void setAuditioninformation3(String auditioninformation3) {
        this.auditioninformation3 = auditioninformation3;
    }

    public String getAuditioninformation4() {
        return auditioninformation4;
    }

    public void setAuditioninformation4(String auditioninformation4) {
        this.auditioninformation4 = auditioninformation4;
    }

    public String getAuditioninformation5() {
        return auditioninformation5;
    }

    public void setAuditioninformation5(String auditioninformation5) {
        this.auditioninformation5 = auditioninformation5;
    }

    public String getAuditioninformation6() {
        return auditioninformation6;
    }

    public void setAuditioninformation6(String auditioninformation6) {
        this.auditioninformation6 = auditioninformation6;
    }

    public String getCode4() {
        return code4;
    }

    public void setCode4(String code4) {
        this.code4 = code4;
    }

    public String getCode5() {
        return code5;
    }

    public void setCode5(String code5) {
        this.code5 = code5;
    }

    public String getCode6() {
        return code6;
    }

    public void setCode6(String code6) {
        this.code6 = code6;
    }

    public String getGrade9geapplicants4() {
        return grade9geapplicants4;
    }

    public void setGrade9geapplicants4(String grade9geapplicants4) {
        this.grade9geapplicants4 = grade9geapplicants4;
    }

    public String getGrade9geapplicants5() {
        return grade9geapplicants5;
    }

    public void setGrade9geapplicants5(String grade9geapplicants5) {
        this.grade9geapplicants5 = grade9geapplicants5;
    }

    public String getGrade9geapplicants6() {
        return grade9geapplicants6;
    }

    public void setGrade9geapplicants6(String grade9geapplicants6) {
        this.grade9geapplicants6 = grade9geapplicants6;
    }

    public String getGrade9geapplicantsperseat4() {
        return grade9geapplicantsperseat4;
    }

    public void setGrade9geapplicantsperseat4(String grade9geapplicantsperseat4) {
        this.grade9geapplicantsperseat4 = grade9geapplicantsperseat4;
    }

    public String getGrade9geapplicantsperseat5() {
        return grade9geapplicantsperseat5;
    }

    public void setGrade9geapplicantsperseat5(String grade9geapplicantsperseat5) {
        this.grade9geapplicantsperseat5 = grade9geapplicantsperseat5;
    }

    public String getGrade9geapplicantsperseat6() {
        return grade9geapplicantsperseat6;
    }

    public void setGrade9geapplicantsperseat6(String grade9geapplicantsperseat6) {
        this.grade9geapplicantsperseat6 = grade9geapplicantsperseat6;
    }

    public String getGrade9gefilledflag4() {
        return grade9gefilledflag4;
    }

    public void setGrade9gefilledflag4(String grade9gefilledflag4) {
        this.grade9gefilledflag4 = grade9gefilledflag4;
    }

    public String getGrade9gefilledflag5() {
        return grade9gefilledflag5;
    }

    public void setGrade9gefilledflag5(String grade9gefilledflag5) {
        this.grade9gefilledflag5 = grade9gefilledflag5;
    }

    public String getGrade9gefilledflag6() {
        return grade9gefilledflag6;
    }

    public void setGrade9gefilledflag6(String grade9gefilledflag6) {
        this.grade9gefilledflag6 = grade9gefilledflag6;
    }

    public String getGrade9swdapplicants4() {
        return grade9swdapplicants4;
    }

    public void setGrade9swdapplicants4(String grade9swdapplicants4) {
        this.grade9swdapplicants4 = grade9swdapplicants4;
    }

    public String getGrade9swdapplicants5() {
        return grade9swdapplicants5;
    }

    public void setGrade9swdapplicants5(String grade9swdapplicants5) {
        this.grade9swdapplicants5 = grade9swdapplicants5;
    }

    public String getGrade9swdapplicants6() {
        return grade9swdapplicants6;
    }

    public void setGrade9swdapplicants6(String grade9swdapplicants6) {
        this.grade9swdapplicants6 = grade9swdapplicants6;
    }

    public String getGrade9swdapplicantsperseat4() {
        return grade9swdapplicantsperseat4;
    }

    public void setGrade9swdapplicantsperseat4(String grade9swdapplicantsperseat4) {
        this.grade9swdapplicantsperseat4 = grade9swdapplicantsperseat4;
    }

    public String getGrade9swdapplicantsperseat5() {
        return grade9swdapplicantsperseat5;
    }

    public void setGrade9swdapplicantsperseat5(String grade9swdapplicantsperseat5) {
        this.grade9swdapplicantsperseat5 = grade9swdapplicantsperseat5;
    }

    public String getGrade9swdapplicantsperseat6() {
        return grade9swdapplicantsperseat6;
    }

    public void setGrade9swdapplicantsperseat6(String grade9swdapplicantsperseat6) {
        this.grade9swdapplicantsperseat6 = grade9swdapplicantsperseat6;
    }

    public String getGrade9swdfilledflag4() {
        return grade9swdfilledflag4;
    }

    public void setGrade9swdfilledflag4(String grade9swdfilledflag4) {
        this.grade9swdfilledflag4 = grade9swdfilledflag4;
    }

    public String getGrade9swdfilledflag5() {
        return grade9swdfilledflag5;
    }

    public void setGrade9swdfilledflag5(String grade9swdfilledflag5) {
        this.grade9swdfilledflag5 = grade9swdfilledflag5;
    }

    public String getGrade9swdfilledflag6() {
        return grade9swdfilledflag6;
    }

    public void setGrade9swdfilledflag6(String grade9swdfilledflag6) {
        this.grade9swdfilledflag6 = grade9swdfilledflag6;
    }

    public String getInterest4() {
        return interest4;
    }

    public void setInterest4(String interest4) {
        this.interest4 = interest4;
    }

    public String getInterest5() {
        return interest5;
    }

    public void setInterest5(String interest5) {
        this.interest5 = interest5;
    }

    public String getInterest6() {
        return interest6;
    }

    public void setInterest6(String interest6) {
        this.interest6 = interest6;
    }

    public String getMethod4() {
        return method4;
    }

    public void setMethod4(String method4) {
        this.method4 = method4;
    }

    public String getMethod5() {
        return method5;
    }

    public void setMethod5(String method5) {
        this.method5 = method5;
    }

    public String getMethod6() {
        return method6;
    }

    public void setMethod6(String method6) {
        this.method6 = method6;
    }

    public String getPrgdesc4() {
        return prgdesc4;
    }

    public void setPrgdesc4(String prgdesc4) {
        this.prgdesc4 = prgdesc4;
    }

    public String getPrgdesc5() {
        return prgdesc5;
    }

    public void setPrgdesc5(String prgdesc5) {
        this.prgdesc5 = prgdesc5;
    }

    public String getPrgdesc6() {
        return prgdesc6;
    }

    public void setPrgdesc6(String prgdesc6) {
        this.prgdesc6 = prgdesc6;
    }

    public String getProgram4() {
        return program4;
    }

    public void setProgram4(String program4) {
        this.program4 = program4;
    }

    public String getProgram5() {
        return program5;
    }

    public void setProgram5(String program5) {
        this.program5 = program5;
    }

    public String getProgram6() {
        return program6;
    }

    public void setProgram6(String program6) {
        this.program6 = program6;
    }

    public String getRequirement1_3() {
        return requirement1_3;
    }

    public void setRequirement1_3(String requirement1_3) {
        this.requirement1_3 = requirement1_3;
    }

    public String getRequirement1_4() {
        return requirement1_4;
    }

    public void setRequirement1_4(String requirement1_4) {
        this.requirement1_4 = requirement1_4;
    }

    public String getRequirement1_5() {
        return requirement1_5;
    }

    public void setRequirement1_5(String requirement1_5) {
        this.requirement1_5 = requirement1_5;
    }

    public String getRequirement1_6() {
        return requirement1_6;
    }

    public void setRequirement1_6(String requirement1_6) {
        this.requirement1_6 = requirement1_6;
    }

    public String getRequirement2_3() {
        return requirement2_3;
    }

    public void setRequirement2_3(String requirement2_3) {
        this.requirement2_3 = requirement2_3;
    }

    public String getRequirement2_4() {
        return requirement2_4;
    }

    public void setRequirement2_4(String requirement2_4) {
        this.requirement2_4 = requirement2_4;
    }

    public String getRequirement2_5() {
        return requirement2_5;
    }

    public void setRequirement2_5(String requirement2_5) {
        this.requirement2_5 = requirement2_5;
    }

    public String getRequirement2_6() {
        return requirement2_6;
    }

    public void setRequirement2_6(String requirement2_6) {
        this.requirement2_6 = requirement2_6;
    }

    public String getRequirement3_3() {
        return requirement3_3;
    }

    public void setRequirement3_3(String requirement3_3) {
        this.requirement3_3 = requirement3_3;
    }

    public String getRequirement3_4() {
        return requirement3_4;
    }

    public void setRequirement3_4(String requirement3_4) {
        this.requirement3_4 = requirement3_4;
    }

    public String getRequirement3_5() {
        return requirement3_5;
    }

    public void setRequirement3_5(String requirement3_5) {
        this.requirement3_5 = requirement3_5;
    }

    public String getRequirement3_6() {
        return requirement3_6;
    }

    public void setRequirement3_6(String requirement3_6) {
        this.requirement3_6 = requirement3_6;
    }

    public String getRequirement4_2() {
        return requirement4_2;
    }

    public void setRequirement4_2(String requirement4_2) {
        this.requirement4_2 = requirement4_2;
    }

    public String getRequirement4_3() {
        return requirement4_3;
    }

    public void setRequirement4_3(String requirement4_3) {
        this.requirement4_3 = requirement4_3;
    }

    public String getRequirement4_4() {
        return requirement4_4;
    }

    public void setRequirement4_4(String requirement4_4) {
        this.requirement4_4 = requirement4_4;
    }

    public String getRequirement4_5() {
        return requirement4_5;
    }

    public void setRequirement4_5(String requirement4_5) {
        this.requirement4_5 = requirement4_5;
    }

    public String getRequirement4_6() {
        return requirement4_6;
    }

    public void setRequirement4_6(String requirement4_6) {
        this.requirement4_6 = requirement4_6;
    }

    public String getSeats104() {
        return seats104;
    }

    public void setSeats104(String seats104) {
        this.seats104 = seats104;
    }

    public String getSeats105() {
        return seats105;
    }

    public void setSeats105(String seats105) {
        this.seats105 = seats105;
    }

    public String getSeats106() {
        return seats106;
    }

    public void setSeats106(String seats106) {
        this.seats106 = seats106;
    }

    public String getSeats9ge4() {
        return seats9ge4;
    }

    public void setSeats9ge4(String seats9ge4) {
        this.seats9ge4 = seats9ge4;
    }

    public String getSeats9ge5() {
        return seats9ge5;
    }

    public void setSeats9ge5(String seats9ge5) {
        this.seats9ge5 = seats9ge5;
    }

    public String getSeats9ge6() {
        return seats9ge6;
    }

    public void setSeats9ge6(String seats9ge6) {
        this.seats9ge6 = seats9ge6;
    }

    public String getSeats9swd4() {
        return seats9swd4;
    }

    public void setSeats9swd4(String seats9swd4) {
        this.seats9swd4 = seats9swd4;
    }

    public String getSeats9swd5() {
        return seats9swd5;
    }

    public void setSeats9swd5(String seats9swd5) {
        this.seats9swd5 = seats9swd5;
    }

    public String getSeats9swd6() {
        return seats9swd6;
    }

    public void setSeats9swd6(String seats9swd6) {
        this.seats9swd6 = seats9swd6;
    }

    public String getEarlycollege() {
        return earlycollege;
    }

    public void setEarlycollege(String earlycollege) {
        this.earlycollege = earlycollege;
    }

    public String getAdmissionspriority51() {
        return admissionspriority51;
    }

    public void setAdmissionspriority51(String admissionspriority51) {
        this.admissionspriority51 = admissionspriority51;
    }

    public String getAdmissionspriority61() {
        return admissionspriority61;
    }

    public void setAdmissionspriority61(String admissionspriority61) {
        this.admissionspriority61 = admissionspriority61;
    }

    public String getAdmissionspriority23() {
        return admissionspriority23;
    }

    public void setAdmissionspriority23(String admissionspriority23) {
        this.admissionspriority23 = admissionspriority23;
    }

    public String getAdmissionspriority24() {
        return admissionspriority24;
    }

    public void setAdmissionspriority24(String admissionspriority24) {
        this.admissionspriority24 = admissionspriority24;
    }

    public String getOffer_rate3() {
        return offer_rate3;
    }

    public void setOffer_rate3(String offer_rate3) {
        this.offer_rate3 = offer_rate3;
    }

    public String getOffer_rate4() {
        return offer_rate4;
    }

    public void setOffer_rate4(String offer_rate4) {
        this.offer_rate4 = offer_rate4;
    }

    public String getPtech() {
        return ptech;
    }

    public void setPtech(String ptech) {
        this.ptech = ptech;
    }

    public String getAdmissionspriority17() {
        return admissionspriority17;
    }

    public void setAdmissionspriority17(String admissionspriority17) {
        this.admissionspriority17 = admissionspriority17;
    }

    public String getAdmissionspriority25() {
        return admissionspriority25;
    }

    public void setAdmissionspriority25(String admissionspriority25) {
        this.admissionspriority25 = admissionspriority25;
    }

    public String getAdmissionspriority26() {
        return admissionspriority26;
    }

    public void setAdmissionspriority26(String admissionspriority26) {
        this.admissionspriority26 = admissionspriority26;
    }

    public String getAdmissionspriority34() {
        return admissionspriority34;
    }

    public void setAdmissionspriority34(String admissionspriority34) {
        this.admissionspriority34 = admissionspriority34;
    }

    public String getAdmissionspriority44() {
        return admissionspriority44;
    }

    public void setAdmissionspriority44(String admissionspriority44) {
        this.admissionspriority44 = admissionspriority44;
    }

    public String getCode7() {
        return code7;
    }

    public void setCode7(String code7) {
        this.code7 = code7;
    }

    public String getGrade9geapplicants7() {
        return grade9geapplicants7;
    }

    public void setGrade9geapplicants7(String grade9geapplicants7) {
        this.grade9geapplicants7 = grade9geapplicants7;
    }

    public String getGrade9geapplicantsperseat7() {
        return grade9geapplicantsperseat7;
    }

    public void setGrade9geapplicantsperseat7(String grade9geapplicantsperseat7) {
        this.grade9geapplicantsperseat7 = grade9geapplicantsperseat7;
    }

    public String getGrade9gefilledflag7() {
        return grade9gefilledflag7;
    }

    public void setGrade9gefilledflag7(String grade9gefilledflag7) {
        this.grade9gefilledflag7 = grade9gefilledflag7;
    }

    public String getGrade9swdapplicants7() {
        return grade9swdapplicants7;
    }

    public void setGrade9swdapplicants7(String grade9swdapplicants7) {
        this.grade9swdapplicants7 = grade9swdapplicants7;
    }

    public String getGrade9swdapplicantsperseat7() {
        return grade9swdapplicantsperseat7;
    }

    public void setGrade9swdapplicantsperseat7(String grade9swdapplicantsperseat7) {
        this.grade9swdapplicantsperseat7 = grade9swdapplicantsperseat7;
    }

    public String getGrade9swdfilledflag7() {
        return grade9swdfilledflag7;
    }

    public void setGrade9swdfilledflag7(String grade9swdfilledflag7) {
        this.grade9swdfilledflag7 = grade9swdfilledflag7;
    }

    public String getInterest7() {
        return interest7;
    }

    public void setInterest7(String interest7) {
        this.interest7 = interest7;
    }

    public String getMethod7() {
        return method7;
    }

    public void setMethod7(String method7) {
        this.method7 = method7;
    }

    public String getOffer_rate5() {
        return offer_rate5;
    }

    public void setOffer_rate5(String offer_rate5) {
        this.offer_rate5 = offer_rate5;
    }

    public String getOffer_rate6() {
        return offer_rate6;
    }

    public void setOffer_rate6(String offer_rate6) {
        this.offer_rate6 = offer_rate6;
    }

    public String getProgram7() {
        return program7;
    }

    public void setProgram7(String program7) {
        this.program7 = program7;
    }

    public String getSeats107() {
        return seats107;
    }

    public void setSeats107(String seats107) {
        this.seats107 = seats107;
    }

    public String getSeats9ge7() {
        return seats9ge7;
    }

    public void setSeats9ge7(String seats9ge7) {
        this.seats9ge7 = seats9ge7;
    }

    public String getSeats9swd7() {
        return seats9swd7;
    }

    public void setSeats9swd7(String seats9swd7) {
        this.seats9swd7 = seats9swd7;
    }

    public String getBoys() {
        return boys;
    }

    public void setBoys(String boys) {
        this.boys = boys;
    }

    public String getDirections2() {
        return directions2;
    }

    public void setDirections2(String directions2) {
        this.directions2 = directions2;
    }

    public String getRequirement5_2() {
        return requirement5_2;
    }

    public void setRequirement5_2(String requirement5_2) {
        this.requirement5_2 = requirement5_2;
    }

    public String getAdmissionspriority18() {
        return admissionspriority18;
    }

    public void setAdmissionspriority18(String admissionspriority18) {
        this.admissionspriority18 = admissionspriority18;
    }

    public String getCode8() {
        return code8;
    }

    public void setCode8(String code8) {
        this.code8 = code8;
    }

    public String getDirections7() {
        return directions7;
    }

    public void setDirections7(String directions7) {
        this.directions7 = directions7;
    }

    public String getGrade9geapplicants8() {
        return grade9geapplicants8;
    }

    public void setGrade9geapplicants8(String grade9geapplicants8) {
        this.grade9geapplicants8 = grade9geapplicants8;
    }

    public String getGrade9geapplicantsperseat8() {
        return grade9geapplicantsperseat8;
    }

    public void setGrade9geapplicantsperseat8(String grade9geapplicantsperseat8) {
        this.grade9geapplicantsperseat8 = grade9geapplicantsperseat8;
    }

    public String getGrade9gefilledflag8() {
        return grade9gefilledflag8;
    }

    public void setGrade9gefilledflag8(String grade9gefilledflag8) {
        this.grade9gefilledflag8 = grade9gefilledflag8;
    }

    public String getGrade9swdapplicants8() {
        return grade9swdapplicants8;
    }

    public void setGrade9swdapplicants8(String grade9swdapplicants8) {
        this.grade9swdapplicants8 = grade9swdapplicants8;
    }

    public String getGrade9swdapplicantsperseat8() {
        return grade9swdapplicantsperseat8;
    }

    public void setGrade9swdapplicantsperseat8(String grade9swdapplicantsperseat8) {
        this.grade9swdapplicantsperseat8 = grade9swdapplicantsperseat8;
    }

    public String getGrade9swdfilledflag8() {
        return grade9swdfilledflag8;
    }

    public void setGrade9swdfilledflag8(String grade9swdfilledflag8) {
        this.grade9swdfilledflag8 = grade9swdfilledflag8;
    }

    public String getInterest8() {
        return interest8;
    }

    public void setInterest8(String interest8) {
        this.interest8 = interest8;
    }

    public String getMethod8() {
        return method8;
    }

    public void setMethod8(String method8) {
        this.method8 = method8;
    }

    public String getPrgdesc7() {
        return prgdesc7;
    }

    public void setPrgdesc7(String prgdesc7) {
        this.prgdesc7 = prgdesc7;
    }

    public String getProgram8() {
        return program8;
    }

    public void setProgram8(String program8) {
        this.program8 = program8;
    }

    public String getRequirement1_7() {
        return requirement1_7;
    }

    public void setRequirement1_7(String requirement1_7) {
        this.requirement1_7 = requirement1_7;
    }

    public String getSeats108() {
        return seats108;
    }

    public void setSeats108(String seats108) {
        this.seats108 = seats108;
    }

    public String getSeats9ge8() {
        return seats9ge8;
    }

    public void setSeats9ge8(String seats9ge8) {
        this.seats9ge8 = seats9ge8;
    }

    public String getSeats9swd8() {
        return seats9swd8;
    }

    public void setSeats9swd8(String seats9swd8) {
        this.seats9swd8 = seats9swd8;
    }

    public String getCommon_audition1() {
        return common_audition1;
    }

    public void setCommon_audition1(String common_audition1) {
        this.common_audition1 = common_audition1;
    }

    public String getGeoeligibility() {
        return geoeligibility;
    }

    public void setGeoeligibility(String geoeligibility) {
        this.geoeligibility = geoeligibility;
    }

    public String getApplicants1specialized() {
        return applicants1specialized;
    }

    public void setApplicants1specialized(String applicants1specialized) {
        this.applicants1specialized = applicants1specialized;
    }

    public String getAppperseat1specialized() {
        return appperseat1specialized;
    }

    public void setAppperseat1specialized(String appperseat1specialized) {
        this.appperseat1specialized = appperseat1specialized;
    }

    public String getSeats1specialized() {
        return seats1specialized;
    }

    public void setSeats1specialized(String seats1specialized) {
        this.seats1specialized = seats1specialized;
    }

    public String getSpecialized() {
        return specialized;
    }

    public void setSpecialized(String specialized) {
        this.specialized = specialized;
    }

    public String getAdmissionspriority42() {
        return admissionspriority42;
    }

    public void setAdmissionspriority42(String admissionspriority42) {
        this.admissionspriority42 = admissionspriority42;
    }

    public String getAdmissionspriority52() {
        return admissionspriority52;
    }

    public void setAdmissionspriority52(String admissionspriority52) {
        this.admissionspriority52 = admissionspriority52;
    }

    public String getCommon_audition4() {
        return common_audition4;
    }

    public void setCommon_audition4(String common_audition4) {
        this.common_audition4 = common_audition4;
    }

    public String getCommon_audition5() {
        return common_audition5;
    }

    public void setCommon_audition5(String common_audition5) {
        this.common_audition5 = common_audition5;
    }

    public String getRequirement5_3() {
        return requirement5_3;
    }

    public void setRequirement5_3(String requirement5_3) {
        this.requirement5_3 = requirement5_3;
    }

    public String getRequirement6_3() {
        return requirement6_3;
    }

    public void setRequirement6_3(String requirement6_3) {
        this.requirement6_3 = requirement6_3;
    }

    public String getRequirement2_7() {
        return requirement2_7;
    }

    public void setRequirement2_7(String requirement2_7) {
        this.requirement2_7 = requirement2_7;
    }

    public String getRequirement3_7() {
        return requirement3_7;
    }

    public void setRequirement3_7(String requirement3_7) {
        this.requirement3_7 = requirement3_7;
    }

    public String getAdmissionspriority71() {
        return admissionspriority71;
    }

    public void setAdmissionspriority71(String admissionspriority71) {
        this.admissionspriority71 = admissionspriority71;
    }

    public String getEligibility3() {
        return eligibility3;
    }

    public void setEligibility3(String eligibility3) {
        this.eligibility3 = eligibility3;
    }

    public String getAdmissionspriority35() {
        return admissionspriority35;
    }

    public void setAdmissionspriority35(String admissionspriority35) {
        this.admissionspriority35 = admissionspriority35;
    }

    public String getRequirement6_2() {
        return requirement6_2;
    }

    public void setRequirement6_2(String requirement6_2) {
        this.requirement6_2 = requirement6_2;
    }

    public String getRequirement5_4() {
        return requirement5_4;
    }

    public void setRequirement5_4(String requirement5_4) {
        this.requirement5_4 = requirement5_4;
    }

    public String getCommon_audition2() {
        return common_audition2;
    }

    public void setCommon_audition2(String common_audition2) {
        this.common_audition2 = common_audition2;
    }

    public String getAdmissionspriority33() {
        return admissionspriority33;
    }

    public void setAdmissionspriority33(String admissionspriority33) {
        this.admissionspriority33 = admissionspriority33;
    }

    public String getDirections3() {
        return directions3;
    }

    public void setDirections3(String directions3) {
        this.directions3 = directions3;
    }

    public String getDirections4() {
        return directions4;
    }

    public void setDirections4(String directions4) {
        this.directions4 = directions4;
    }

    public String getAdmissionspriority36() {
        return admissionspriority36;
    }

    public void setAdmissionspriority36(String admissionspriority36) {
        this.admissionspriority36 = admissionspriority36;
    }

    public String getAdmissionspriority46() {
        return admissionspriority46;
    }

    public void setAdmissionspriority46(String admissionspriority46) {
        this.admissionspriority46 = admissionspriority46;
    }

    public String getAdmissionspriority56() {
        return admissionspriority56;
    }

    public void setAdmissionspriority56(String admissionspriority56) {
        this.admissionspriority56 = admissionspriority56;
    }

    public String getCommon_audition3() {
        return common_audition3;
    }

    public void setCommon_audition3(String common_audition3) {
        this.common_audition3 = common_audition3;
    }

    public String getAdmissionspriority27() {
        return admissionspriority27;
    }

    public void setAdmissionspriority27(String admissionspriority27) {
        this.admissionspriority27 = admissionspriority27;
    }

    public String getAdmissionspriority37() {
        return admissionspriority37;
    }

    public void setAdmissionspriority37(String admissionspriority37) {
        this.admissionspriority37 = admissionspriority37;
    }

    public String getPrgdesc8() {
        return prgdesc8;
    }

    public void setPrgdesc8(String prgdesc8) {
        this.prgdesc8 = prgdesc8;
    }

    public String getAdmissionspriority54() {
        return admissionspriority54;
    }

    public void setAdmissionspriority54(String admissionspriority54) {
        this.admissionspriority54 = admissionspriority54;
    }

    public String getAdmissionspriority64() {
        return admissionspriority64;
    }

    public void setAdmissionspriority64(String admissionspriority64) {
        this.admissionspriority64 = admissionspriority64;
    }

    public String getAdmissionspriority74() {
        return admissionspriority74;
    }

    public void setAdmissionspriority74(String admissionspriority74) {
        this.admissionspriority74 = admissionspriority74;
    }

    public String getRequirement6_1() {
        return requirement6_1;
    }

    public void setRequirement6_1(String requirement6_1) {
        this.requirement6_1 = requirement6_1;
    }

    public String getEligibility4() {
        return eligibility4;
    }

    public void setEligibility4(String eligibility4) {
        this.eligibility4 = eligibility4;
    }

    public String getAuditioninformation7() {
        return auditioninformation7;
    }

    public void setAuditioninformation7(String auditioninformation7) {
        this.auditioninformation7 = auditioninformation7;
    }

    public String getCommon_audition6() {
        return common_audition6;
    }

    public void setCommon_audition6(String common_audition6) {
        this.common_audition6 = common_audition6;
    }

    public String getCommon_audition7() {
        return common_audition7;
    }

    public void setCommon_audition7(String common_audition7) {
        this.common_audition7 = common_audition7;
    }

    public String getEligibility5() {
        return eligibility5;
    }

    public void setEligibility5(String eligibility5) {
        this.eligibility5 = eligibility5;
    }

    public String getEligibility6() {
        return eligibility6;
    }

    public void setEligibility6(String eligibility6) {
        this.eligibility6 = eligibility6;
    }

    public String getEligibility7() {
        return eligibility7;
    }

    public void setEligibility7(String eligibility7) {
        this.eligibility7 = eligibility7;
    }

    public String getRequirement4_7() {
        return requirement4_7;
    }

    public void setRequirement4_7(String requirement4_7) {
        this.requirement4_7 = requirement4_7;
    }

    public String getRequirement5_5() {
        return requirement5_5;
    }

    public void setRequirement5_5(String requirement5_5) {
        this.requirement5_5 = requirement5_5;
    }

    public String getAdmissionspriority110() {
        return admissionspriority110;
    }

    public void setAdmissionspriority110(String admissionspriority110) {
        this.admissionspriority110 = admissionspriority110;
    }

    public String getAdmissionspriority19() {
        return admissionspriority19;
    }

    public void setAdmissionspriority19(String admissionspriority19) {
        this.admissionspriority19 = admissionspriority19;
    }

    public String getAdmissionspriority28() {
        return admissionspriority28;
    }

    public void setAdmissionspriority28(String admissionspriority28) {
        this.admissionspriority28 = admissionspriority28;
    }

    public String getAdmissionspriority29() {
        return admissionspriority29;
    }

    public void setAdmissionspriority29(String admissionspriority29) {
        this.admissionspriority29 = admissionspriority29;
    }

    public String getCode10() {
        return code10;
    }

    public void setCode10(String code10) {
        this.code10 = code10;
    }

    public String getCode9() {
        return code9;
    }

    public void setCode9(String code9) {
        this.code9 = code9;
    }

    public String getGrade9geapplicants10() {
        return grade9geapplicants10;
    }

    public void setGrade9geapplicants10(String grade9geapplicants10) {
        this.grade9geapplicants10 = grade9geapplicants10;
    }

    public String getGrade9geapplicants9() {
        return grade9geapplicants9;
    }

    public void setGrade9geapplicants9(String grade9geapplicants9) {
        this.grade9geapplicants9 = grade9geapplicants9;
    }

    public String getGrade9geapplicantsperseat10() {
        return grade9geapplicantsperseat10;
    }

    public void setGrade9geapplicantsperseat10(String grade9geapplicantsperseat10) {
        this.grade9geapplicantsperseat10 = grade9geapplicantsperseat10;
    }

    public String getGrade9geapplicantsperseat9() {
        return grade9geapplicantsperseat9;
    }

    public void setGrade9geapplicantsperseat9(String grade9geapplicantsperseat9) {
        this.grade9geapplicantsperseat9 = grade9geapplicantsperseat9;
    }

    public String getGrade9gefilledflag10() {
        return grade9gefilledflag10;
    }

    public void setGrade9gefilledflag10(String grade9gefilledflag10) {
        this.grade9gefilledflag10 = grade9gefilledflag10;
    }

    public String getGrade9gefilledflag9() {
        return grade9gefilledflag9;
    }

    public void setGrade9gefilledflag9(String grade9gefilledflag9) {
        this.grade9gefilledflag9 = grade9gefilledflag9;
    }

    public String getGrade9swdapplicants10() {
        return grade9swdapplicants10;
    }

    public void setGrade9swdapplicants10(String grade9swdapplicants10) {
        this.grade9swdapplicants10 = grade9swdapplicants10;
    }

    public String getGrade9swdapplicants9() {
        return grade9swdapplicants9;
    }

    public void setGrade9swdapplicants9(String grade9swdapplicants9) {
        this.grade9swdapplicants9 = grade9swdapplicants9;
    }

    public String getGrade9swdapplicantsperseat10() {
        return grade9swdapplicantsperseat10;
    }

    public void setGrade9swdapplicantsperseat10(String grade9swdapplicantsperseat10) {
        this.grade9swdapplicantsperseat10 = grade9swdapplicantsperseat10;
    }

    public String getGrade9swdapplicantsperseat9() {
        return grade9swdapplicantsperseat9;
    }

    public void setGrade9swdapplicantsperseat9(String grade9swdapplicantsperseat9) {
        this.grade9swdapplicantsperseat9 = grade9swdapplicantsperseat9;
    }

    public String getGrade9swdfilledflag10() {
        return grade9swdfilledflag10;
    }

    public void setGrade9swdfilledflag10(String grade9swdfilledflag10) {
        this.grade9swdfilledflag10 = grade9swdfilledflag10;
    }

    public String getGrade9swdfilledflag9() {
        return grade9swdfilledflag9;
    }

    public void setGrade9swdfilledflag9(String grade9swdfilledflag9) {
        this.grade9swdfilledflag9 = grade9swdfilledflag9;
    }

    public String getInterest10() {
        return interest10;
    }

    public void setInterest10(String interest10) {
        this.interest10 = interest10;
    }

    public String getInterest9() {
        return interest9;
    }

    public void setInterest9(String interest9) {
        this.interest9 = interest9;
    }

    public String getMethod10() {
        return method10;
    }

    public void setMethod10(String method10) {
        this.method10 = method10;
    }

    public String getMethod9() {
        return method9;
    }

    public void setMethod9(String method9) {
        this.method9 = method9;
    }

    public String getOffer_rate7() {
        return offer_rate7;
    }

    public void setOffer_rate7(String offer_rate7) {
        this.offer_rate7 = offer_rate7;
    }

    public String getOffer_rate8() {
        return offer_rate8;
    }

    public void setOffer_rate8(String offer_rate8) {
        this.offer_rate8 = offer_rate8;
    }

    public String getOffer_rate9() {
        return offer_rate9;
    }

    public void setOffer_rate9(String offer_rate9) {
        this.offer_rate9 = offer_rate9;
    }

    public String getPrgdesc10() {
        return prgdesc10;
    }

    public void setPrgdesc10(String prgdesc10) {
        this.prgdesc10 = prgdesc10;
    }

    public String getPrgdesc9() {
        return prgdesc9;
    }

    public void setPrgdesc9(String prgdesc9) {
        this.prgdesc9 = prgdesc9;
    }

    public String getProgram10() {
        return program10;
    }

    public void setProgram10(String program10) {
        this.program10 = program10;
    }

    public String getProgram9() {
        return program9;
    }

    public void setProgram9(String program9) {
        this.program9 = program9;
    }

    public String getRequirement1_8() {
        return requirement1_8;
    }

    public void setRequirement1_8(String requirement1_8) {
        this.requirement1_8 = requirement1_8;
    }

    public String getRequirement2_8() {
        return requirement2_8;
    }

    public void setRequirement2_8(String requirement2_8) {
        this.requirement2_8 = requirement2_8;
    }

    public String getRequirement3_8() {
        return requirement3_8;
    }

    public void setRequirement3_8(String requirement3_8) {
        this.requirement3_8 = requirement3_8;
    }

    public String getSeats1010() {
        return seats1010;
    }

    public void setSeats1010(String seats1010) {
        this.seats1010 = seats1010;
    }

    public String getSeats109() {
        return seats109;
    }

    public void setSeats109(String seats109) {
        this.seats109 = seats109;
    }

    public String getSeats9ge10() {
        return seats9ge10;
    }

    public void setSeats9ge10(String seats9ge10) {
        this.seats9ge10 = seats9ge10;
    }

    public String getSeats9ge9() {
        return seats9ge9;
    }

    public void setSeats9ge9(String seats9ge9) {
        this.seats9ge9 = seats9ge9;
    }

    public String getSeats9swd10() {
        return seats9swd10;
    }

    public void setSeats9swd10(String seats9swd10) {
        this.seats9swd10 = seats9swd10;
    }

    public String getSeats9swd9() {
        return seats9swd9;
    }

    public void setSeats9swd9(String seats9swd9) {
        this.seats9swd9 = seats9swd9;
    }

    public String getAdmissionspriority43() {
        return admissionspriority43;
    }

    public void setAdmissionspriority43(String admissionspriority43) {
        this.admissionspriority43 = admissionspriority43;
    }

    public String getApplicants2specialized() {
        return applicants2specialized;
    }

    public void setApplicants2specialized(String applicants2specialized) {
        this.applicants2specialized = applicants2specialized;
    }

    public String getApplicants3specialized() {
        return applicants3specialized;
    }

    public void setApplicants3specialized(String applicants3specialized) {
        this.applicants3specialized = applicants3specialized;
    }

    public String getApplicants4specialized() {
        return applicants4specialized;
    }

    public void setApplicants4specialized(String applicants4specialized) {
        this.applicants4specialized = applicants4specialized;
    }

    public String getApplicants5specialized() {
        return applicants5specialized;
    }

    public void setApplicants5specialized(String applicants5specialized) {
        this.applicants5specialized = applicants5specialized;
    }

    public String getApplicants6specialized() {
        return applicants6specialized;
    }

    public void setApplicants6specialized(String applicants6specialized) {
        this.applicants6specialized = applicants6specialized;
    }

    public String getAppperseat2specialized() {
        return appperseat2specialized;
    }

    public void setAppperseat2specialized(String appperseat2specialized) {
        this.appperseat2specialized = appperseat2specialized;
    }

    public String getAppperseat3specialized() {
        return appperseat3specialized;
    }

    public void setAppperseat3specialized(String appperseat3specialized) {
        this.appperseat3specialized = appperseat3specialized;
    }

    public String getAppperseat4specialized() {
        return appperseat4specialized;
    }

    public void setAppperseat4specialized(String appperseat4specialized) {
        this.appperseat4specialized = appperseat4specialized;
    }

    public String getAppperseat5specialized() {
        return appperseat5specialized;
    }

    public void setAppperseat5specialized(String appperseat5specialized) {
        this.appperseat5specialized = appperseat5specialized;
    }

    public String getAppperseat6specialized() {
        return appperseat6specialized;
    }

    public void setAppperseat6specialized(String appperseat6specialized) {
        this.appperseat6specialized = appperseat6specialized;
    }

    public String getDirections5() {
        return directions5;
    }

    public void setDirections5(String directions5) {
        this.directions5 = directions5;
    }

    public String getDirections6() {
        return directions6;
    }

    public void setDirections6(String directions6) {
        this.directions6 = directions6;
    }

    public String getRequirement5_6() {
        return requirement5_6;
    }

    public void setRequirement5_6(String requirement5_6) {
        this.requirement5_6 = requirement5_6;
    }

    public String getSeats2specialized() {
        return seats2specialized;
    }

    public void setSeats2specialized(String seats2specialized) {
        this.seats2specialized = seats2specialized;
    }

    public String getSeats3specialized() {
        return seats3specialized;
    }

    public void setSeats3specialized(String seats3specialized) {
        this.seats3specialized = seats3specialized;
    }

    public String getSeats4specialized() {
        return seats4specialized;
    }

    public void setSeats4specialized(String seats4specialized) {
        this.seats4specialized = seats4specialized;
    }

    public String getSeats5specialized() {
        return seats5specialized;
    }

    public void setSeats5specialized(String seats5specialized) {
        this.seats5specialized = seats5specialized;
    }

    public String getSeats6specialized() {
        return seats6specialized;
    }

    public void setSeats6specialized(String seats6specialized) {
        this.seats6specialized = seats6specialized;
    }

    public String getAdmissionspriority53() {
        return admissionspriority53;
    }

    public void setAdmissionspriority53(String admissionspriority53) {
        this.admissionspriority53 = admissionspriority53;
    }

    public String getAdmissionspriority62() {
        return admissionspriority62;
    }

    public void setAdmissionspriority62(String admissionspriority62) {
        this.admissionspriority62 = admissionspriority62;
    }

    public String getAdmissionspriority63() {
        return admissionspriority63;
    }

    public void setAdmissionspriority63(String admissionspriority63) {
        this.admissionspriority63 = admissionspriority63;
    }

    public String getRequirement5_7() {
        return requirement5_7;
    }

    public void setRequirement5_7(String requirement5_7) {
        this.requirement5_7 = requirement5_7;
    }

    public String getRequirement6_7() {
        return requirement6_7;
    }

    public void setRequirement6_7(String requirement6_7) {
        this.requirement6_7 = requirement6_7;
    }

}


