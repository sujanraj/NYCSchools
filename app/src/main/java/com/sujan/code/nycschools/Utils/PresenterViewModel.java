package com.sujan.code.nycschools.Utils;

import android.arch.lifecycle.ViewModel;

import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SATRecords;
import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SchoolsData;
import com.sujan.code.nycschools.Presenter.MainPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of new Android App Architecture using ViewModel.
 */
public class PresenterViewModel extends ViewModel {
    List<SchoolsData> schoolsData;
    List<SATRecords> satRecords;
    private MainPresenter mainPresenter;

    public MainPresenter getMainPresenter() {
        if (mainPresenter != null) {
            return mainPresenter;
        } else {
            mainPresenter = new MainPresenter();
        }
        return mainPresenter;
    }

    public List<SchoolsData> getSchoolsData() {
        if (schoolsData != null) {
            return schoolsData;
        } else {
            return new ArrayList<SchoolsData>();
        }
    }

    public void setSchoolsData(List<SchoolsData> data) {
        schoolsData = data;

    }

    public List<SATRecords> getSatRecords() {
        if (satRecords != null) {
            return satRecords;
        } else {
            return new ArrayList<SATRecords>();
        }
    }

    public void setSatRecords(List<SATRecords> satRecords) {
        this.satRecords = satRecords;
    }
}
