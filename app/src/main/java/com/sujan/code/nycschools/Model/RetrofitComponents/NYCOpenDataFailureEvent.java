package com.sujan.code.nycschools.Model.RetrofitComponents;

/**
 * Created by HERO on 2/1/2018.
 */

public class NYCOpenDataFailureEvent {

    private String onFailureResponse;

    //    Receives the onFailure Response from the NYCOpenDataParser Failure Event
    public NYCOpenDataFailureEvent(String onFailureResponse) {
        this.onFailureResponse = onFailureResponse;
    }

    //    Method to return the onFailure Response received from NYCOpenDataParser
    public String getOnFailureResponse() {
        return onFailureResponse;
    }
}
