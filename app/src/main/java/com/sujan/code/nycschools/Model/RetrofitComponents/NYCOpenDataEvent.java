package com.sujan.code.nycschools.Model.RetrofitComponents;


import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SchoolsData;

import java.util.List;

/**
 * NYCOpenDataEvent is a model class used by the NYCOpenDataParser class to create an event
 * and update the UI depending on the Network Response.
 */

public class NYCOpenDataEvent {

    private List<SchoolsData> response;

    //    Receives the List<Response> values from the NYCOpenDataParser Event
    public NYCOpenDataEvent(List<SchoolsData> response) {
        this.response = response;
    }

    //    Method to return the List<Response> value received from NYCOpenDataParser
    public List<SchoolsData> getListOfResponse() {
        return response;
    }
}
