package com.sujan.code.nycschools.Model.RetrofitComponents.POJOs;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbookpro on 3/24/18.
 */

public class DetailResponseEvent extends ArrayList<Parcelable> implements Parcelable {
    public static final Creator<DetailResponseEvent> CREATOR = new Creator<DetailResponseEvent>() {
        @Override
        public DetailResponseEvent createFromParcel(Parcel in) {
            return new DetailResponseEvent(in);
        }

        @Override
        public DetailResponseEvent[] newArray(int size) {
            return new DetailResponseEvent[size];
        }
    };
    List<SATRecords> response;
    String dbn;

    public DetailResponseEvent(List<SATRecords> response, String dbn) {
        this.response = response;
        this.dbn = dbn;
    }

    protected DetailResponseEvent(Parcel in) {
        response = in.createTypedArrayList(SATRecords.CREATOR);
        dbn = in.readString();
    }

    public List<SATRecords> getResponse() {
        return response;
    }

    public void setResponse(List<SATRecords> response) {
        this.response = response;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(response);
        dest.writeString(dbn);
    }
}
