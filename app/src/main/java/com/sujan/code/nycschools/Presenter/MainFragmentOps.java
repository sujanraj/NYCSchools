package com.sujan.code.nycschools.Presenter;


import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SchoolsData;

import java.util.List;

public interface MainFragmentOps {
    public void onSuccessOpenNotifyApiResponse(List<SchoolsData> responseList);

    public void onFailureOpenNotifyApiResponse(String onFailureResponse);
}
