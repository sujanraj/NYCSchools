package com.sujan.code.nycschools.View;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SchoolsData;
import com.sujan.code.nycschools.Presenter.MainFragmentOps;
import com.sujan.code.nycschools.Presenter.MainPresenter;
import com.sujan.code.nycschools.R;
import com.sujan.code.nycschools.Utils.PresenterViewModel;

import java.util.List;


public class MainFragment extends Fragment implements MainFragmentOps {

    private MainPresenter mainPresenter;
    private RecyclerView recyclerView;
    private SchoolsRecyclerViewAdapter schoolsRecyclerViewAdapter;
    private Context context;
    private TextView status;
    private ProgressBar progressBar;
    private PresenterViewModel presenterViewModel;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    //  This Method will give presenter OnStart feature of the Fragment
    //  To Support te EventBus feature.

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mainfragment, container, false);
        recyclerView = view.findViewById(R.id.datapresenter);
        status = view.findViewById(R.id.status);
        progressBar = view.findViewById(R.id.progressBar);
        presenterViewModel = ViewModelProviders.of(this).get(PresenterViewModel.class);
        mainPresenter = presenterViewModel.getMainPresenter();
        mainPresenter.buildDagger();
        mainPresenter.makeOpenNotifyAPIcall();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    //This Method will give presenter OnStop feature of the Fragment
    //  To Support te EventBus feature.
    @Override
    public void onStop() {
        super.onStop();

    }


    /**
     * Called when request to Open-Notify.org is a success and response is received.
     * Invoked by MainActivity.
     *
     * @param responseList response from Open-Notify.org api.
     */
    @Override
    public void onSuccessOpenNotifyApiResponse(List<SchoolsData> responseList) {
        Log.e("###", "size " + responseList.size());
        //Passing the Network Response List to RecyclerViewAdapter
        schoolsRecyclerViewAdapter = new SchoolsRecyclerViewAdapter(responseList, mainPresenter);
        //Setting the Layout to RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        // Setting the Adapter to RecyclerView
        recyclerView.setAdapter(schoolsRecyclerViewAdapter);
        status.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);


    }

    /**
     * Called when request to Open-Notify.org is a success and response is received.
     * Invoked by MainPresenter.
     *
     * @param onFailureResponse failed response from the api.
     */
    @Override
    public void onFailureOpenNotifyApiResponse(String onFailureResponse) {
        recyclerView.setVisibility(View.GONE);
        status.setText(onFailureResponse);
        status.setVisibility(View.VISIBLE);

    }

    public SchoolsRecyclerViewAdapter getSchoolsRecyclerViewAdapter() {
        return schoolsRecyclerViewAdapter;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("...", "MAINFRAGMENTonDestroy");
    }
}
