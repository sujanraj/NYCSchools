package com.sujan.code.nycschools;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {
    private TextView overview, name, satReading, satMath, satWriting, phone, address, email;

    //school's details
    private String overviewinfo;
    private String readingAvg;
    private String mathAvg;
    private String writingAvg;
    private String schoolname;
    private String mphone;
    private String maddress;
    private String memail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        overview = findViewById(R.id.overview);
        name = findViewById(R.id.name);
        satReading = findViewById(R.id.satReading);
        satMath = findViewById(R.id.satMath);
        satWriting = findViewById(R.id.satWriting);
        phone = findViewById(R.id.phone);
        address = findViewById(R.id.address);
        email = findViewById(R.id.email);


    }

    /**
     * Received data from intent and initialize variable using the data.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        overviewinfo = intent.getStringExtra("overview");
        readingAvg = intent.getStringExtra("SATReadingAvg");
        mathAvg = intent.getStringExtra("SATMathAvg");
        writingAvg = intent.getStringExtra("SATWritingAvg");
        schoolname = intent.getStringExtra("schoolName");
        mphone = intent.getStringExtra("phone");
        maddress = intent.getStringExtra("address");
        memail = intent.getStringExtra("email");
        populateView();


    }

    /**
     * Update detail view with the data received
     */
    private void populateView() {
        Log.e("....", readingAvg + ".." + mathAvg + ".." + writingAvg);
        overview.setText(overviewinfo);
        name.setText(schoolname);
        if (readingAvg == null) {
            readingAvg = "Not Available.";
        }
        if (mathAvg == null) {
            mathAvg = "Not Available.";
        }
        if (writingAvg == null) {
            writingAvg = "Not Available.";
        }
        satReading.setText("SAT Reading Average: " + readingAvg);
        satMath.setText("SAT Math Average: " + mathAvg);
        satWriting.setText("SAT Writing Average: " + writingAvg);
        phone.setText("Phone no.: " + mphone);
        email.setText("Email: " + memail);
        address.setText("Address: " + maddress);

    }


}
