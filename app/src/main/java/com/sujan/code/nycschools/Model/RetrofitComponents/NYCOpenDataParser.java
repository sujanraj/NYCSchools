package com.sujan.code.nycschools.Model.RetrofitComponents;

import android.util.Log;

import com.sujan.code.nycschools.Model.DaggerComponents.MyApplicationDaggerBuild;
import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.DetailResponseEvent;
import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SATRecords;
import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SchoolsData;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * NYCOpenDataParser is used to pass the latitude and longitude values of the device location
 * to make the network call and get the required response from network.
 */

public class NYCOpenDataParser {

    @Inject
    @Named("Dummy")
    Retrofit retrofitAdapter;
    private NYCOpenDataInterface NYCOpenDataInterface;


    public NYCOpenDataParser() {
        MyApplicationDaggerBuild.getMyApplicationDaggerBuild().getMyApplicationDaggerComponent().inject(this);
        getRetrofitClientReference();
    }

    // Creating the reference for the NYCOpenDataInterface
    public void getRetrofitClientReference() {
        this.NYCOpenDataInterface = retrofitAdapter.create(NYCOpenDataInterface.class);
    }

    /**
     * Make a asynchronous call to the given API endpoint.
     */
    public void getJSONDataFromAPI() {

        // Passing latitude and longitude values
        Call<List<SchoolsData>> call = NYCOpenDataInterface.getSchoolData();

        // Making the network call
        call.enqueue(new Callback<List<SchoolsData>>() {

            // Method that execute when call is successful
            @Override
            public void onResponse(Call<List<SchoolsData>> call, retrofit2.Response<List<SchoolsData>> response) {

                // checking the Network Response for any Error

                if (response.errorBody() != null) {
                    Log.e("Response Body error", "Error-" + response.body().toString());
                } else {

                    // Event that passes the response to presenter and make necessary update on UI
                    EventBus.getDefault().post(new NYCOpenDataEvent(response.body()));

                }
            }

            // Method that execute when call is Failed
            @Override
            public void onFailure(Call<List<SchoolsData>> call, Throwable t) {

                Log.e("406", "Failed" + t.getMessage().toString());
                // Event that passes the OnFailure response to presenter and make necessary update on UI
                EventBus.getDefault().post(new NYCOpenDataFailureEvent("Connection Failed.Internet is not available."));
            }
        });
    }

    /**
     * Make a asynchronous call to the given API endpoint.
     */
    public void getSchoolDetailsFromAPI(final String dbn) {

        // Passing latitude and longitude values
        Call<List<SATRecords>> call = NYCOpenDataInterface.getSchoolSATData();

        // Making the network call
        call.enqueue(new Callback<List<SATRecords>>() {

            // Method that execute when call is successful
            @Override
            public void onResponse(Call<List<SATRecords>> call, retrofit2.Response<List<SATRecords>> response) {

                // checking the Network Response for any Error

                if (response.errorBody() != null) {
                    Log.e("Response Body error", "Error-" + response.body().toString());
                } else {

                    // Event that passes the response to presenter and make necessary update on UI
                    EventBus.getDefault().post(new DetailResponseEvent(response.body(), dbn));

                }
            }

            // Method that execute when call is Failed
            @Override
            public void onFailure(Call<List<SATRecords>> call, Throwable t) {

                Log.e("406", "Failed" + t.getMessage().toString());
                // Event that passes the OnFailure response to presenter and make necessary update on UI
                EventBus.getDefault().post(new NYCOpenDataFailureEvent("Connection Failed.Internet is not available."));
            }
        });
    }
}
