package com.sujan.code.nycschools.Utils.ConnectionUtils;

/**
 * Created by macbookpro on 3/14/18.
 */

public interface ICheckNetwork {
    boolean getNetworkConnectionStatus();
}
