package com.sujan.code.nycschools.RetrofitComponentsTest;

import com.sujan.code.nycschools.Model.RetrofitComponents.NYCOpenDataEvent;
import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SchoolsData;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbookpro on 3/28/18.
 */

public class NYCOpenDataEventTest {
    List<SchoolsData> schoolsDataList;

    @Before
    public void init() {
        schoolsDataList = new ArrayList<>();

    }

    @Test
    public void NYCOpenDataEventConstruct_isSuccess() {
        NYCOpenDataEvent nycOpenDataEvent = new NYCOpenDataEvent(schoolsDataList);
        Assert.assertNotNull(nycOpenDataEvent);
    }

    @Test
    public void getListOfResponse_Returnslist() {
        NYCOpenDataEvent nycOpenDataEvent = new NYCOpenDataEvent(schoolsDataList);
        Assert.assertEquals(schoolsDataList, nycOpenDataEvent.getListOfResponse());

    }
}
