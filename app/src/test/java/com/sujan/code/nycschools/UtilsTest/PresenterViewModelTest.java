package com.sujan.code.nycschools.UtilsTest;

import android.content.Context;

import com.sujan.code.nycschools.Model.RetrofitComponents.POJOs.SATRecords;
import com.sujan.code.nycschools.Utils.PresenterViewModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

public class PresenterViewModelTest {
    Context context;
    PresenterViewModel presenterViewModel;


    @Before
    public void init() {
        context = Mockito.mock(Context.class);
        presenterViewModel = new PresenterViewModel();


    }

    @Test
    public void getMainPresenter_ReturnsNotNull() {
        Assert.assertNotNull(presenterViewModel.getMainPresenter());


    }

    @Test
    public void setSchoolData_setSuccess() {
        presenterViewModel.setSatRecords(new ArrayList<SATRecords>());
        Assert.assertNotNull(presenterViewModel.getSchoolsData());


    }

    @Test
    public void getSatRecords_ReturnsNotNull() {
        Assert.assertNotNull(presenterViewModel.getSatRecords());


    }

    @Test
    public void setSatRecord_setSuccess() {
        presenterViewModel.setSatRecords(new ArrayList<SATRecords>());
        Assert.assertNotNull(presenterViewModel.getSatRecords());


    }

}
