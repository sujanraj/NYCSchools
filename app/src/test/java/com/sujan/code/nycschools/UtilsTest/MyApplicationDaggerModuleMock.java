package com.sujan.code.nycschools.UtilsTest;

import android.content.Context;

import com.sujan.code.nycschools.Model.DaggerComponents.MyApplicationDaggerModule;
import com.sujan.code.nycschools.Model.RetrofitComponents.NYCOpenDataParser;
import com.sujan.code.nycschools.Utils.ConnectionUtils.CheckNetwork;

import org.mockito.Mockito;

import retrofit2.Retrofit;

/**
 * Created by macbookpro on 3/28/18.
 */

public class MyApplicationDaggerModuleMock extends MyApplicationDaggerModule {
    public MyApplicationDaggerModuleMock(Context context) {
        super(context);
    }


    @Override
    public Context provideContext() {
        return Mockito.mock(Context.class);
    }

    @Override
    public Retrofit providesRetrofitAdapter() {
        return Mockito.mock(Retrofit.class);
    }

    @Override
    public Retrofit provideRetrofitForTesting() {
        return Mockito.mock(Retrofit.class);
    }

    @Override
    public NYCOpenDataParser providesOpenNotifyDataParser() {
        return Mockito.mock(NYCOpenDataParser.class);
    }

    @Override
    public CheckNetwork providesInternetCheck(Context context) {
        return Mockito.mock(CheckNetwork.class);
    }


}
