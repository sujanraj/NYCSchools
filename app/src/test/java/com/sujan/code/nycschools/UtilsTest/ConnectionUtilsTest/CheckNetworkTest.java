package com.sujan.code.nycschools.UtilsTest.ConnectionUtilsTest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.sujan.code.nycschools.Utils.ConnectionUtils.CheckNetwork;
import com.sujan.code.nycschools.Utils.ConnectionUtils.ICheckNetwork;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;


public class CheckNetworkTest {


    Context mMockContext;

    ICheckNetwork checkNetwork1;
    ConnectivityManager connManager;
    NetworkInfo networkInfo;
    CheckNetwork checkNetwork;


    @Before
    public void init() {
        mMockContext = Mockito.mock(Context.class);
        mMockContext = Mockito.mock(Context.class);
        connManager = Mockito.mock(ConnectivityManager.class);
        networkInfo = Mockito.mock(NetworkInfo.class);
        checkNetwork1 = Mockito.mock(ICheckNetwork.class);
        checkNetwork = new CheckNetwork(mMockContext);


    }

    @Test
    public void CheckNetworkConstruct_isSuccess() {
//        CheckNetwork checkNetwork=new CheckNetwork(mMockContext);
        Assert.assertNotNull(checkNetwork);

    }

    @Test
    public void getNetworkStatus_ReturnsTrue() {


        //when
        Mockito.when(mMockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(connManager);
        Mockito.when(connManager.getActiveNetworkInfo()).thenReturn(networkInfo);
        Mockito.when(networkInfo.isConnectedOrConnecting()).thenReturn(true);
        //run

        checkNetwork.getNetworkConnectionStatus();

        Mockito.verify(mMockContext).getSystemService(Context.CONNECTIVITY_SERVICE);
        Mockito.verify(connManager).getActiveNetworkInfo();
        Mockito.verify(networkInfo).isConnectedOrConnecting();


        //then
        Assert.assertEquals(true, checkNetwork.getNetworkConnectionStatus());


    }

    @Test
    public void getNetworkStatus_ReturnsFalse() {

        //when
        Mockito.when(mMockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(connManager);
        Mockito.when(connManager.getActiveNetworkInfo()).thenReturn(networkInfo);
        Mockito.when(networkInfo.isConnectedOrConnecting()).thenReturn(false);
        //run

        checkNetwork.getNetworkConnectionStatus();

        Mockito.verify(mMockContext).getSystemService(Context.CONNECTIVITY_SERVICE);
        Mockito.verify(connManager).getActiveNetworkInfo();
        Mockito.verify(networkInfo).isConnectedOrConnecting();


        //then
        Assert.assertEquals(false, checkNetwork.getNetworkConnectionStatus());

    }
}
